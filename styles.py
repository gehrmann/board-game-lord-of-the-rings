#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import os
import sys

from PyQt import QtWidgets

dpmm = QtWidgets.QWidget().physicalDpiX() / 25.4


def mm2px(value):
	return int(value * dpmm)

min_touchable_size = 10.

stylesheet = '''
QPushButton, QToolButton, QComboBox { background: #696; color: #fff; }
QPushButton, QToolButton { min-width: ''' + str(mm2px(min_touchable_size)) + '''px; min-height: ''' + str(mm2px(min_touchable_size)) + '''px; icon-size: ''' + str(mm2px(min_touchable_size * .6)) + '''px; }
QCheckBox::indicator, QGroupBox::indicator { width: ''' + str(mm2px(min_touchable_size)) + '''px; height: ''' + str(mm2px(min_touchable_size)) + '''px; }
QLineEdit { min-height: ''' + str(mm2px(min_touchable_size)) + '''px; border: 1px solid #666; border-radius: 4px; }
QComboBox:editable { padding: ''' + str(mm2px(1)) + '''px; }
QComboBox::drop-down { min-width: ''' + str(mm2px(8)) + '''px; border: none 0px transparent; }
QTextBrowser, QTableView, QTableWidget { border: 0px none transparent; background: transparent; }

#scene_viewer QScrollBar { background: #ddd; margin: 0px 0px 0px 0px; }
#scene_viewer QScrollBar:vertical { width: ''' + str(mm2px(1)) + '''px; }
#scene_viewer QScrollBar:horizontal { height: ''' + str(mm2px(1)) + '''px; }
#scene_viewer QScrollBar::handle { background: #696; }
#scene_viewer QScrollBar::sub-page, QScrollBar::add-page { background: transparent; }
#scene_viewer QScrollBar::sub-line, #scene_viewer QScrollBar::add-line { background: transparent; }
/*
#scene_viewer QScrollBar { background: #ddd; margin: 0px 0px 0px 0px; }
#scene_viewer QScrollBar:vertical { width: ''' + str(mm2px(6)) + '''px; }
#scene_viewer QScrollBar:horizontal { height: ''' + str(mm2px(6)) + '''px; }
#scene_viewer QScrollBar::handle:verical { min-height: ''' + str(mm2px(6)) + '''px; margin: ''' + str(mm2px(6.2)) + '''px 0; }
#scene_viewer QScrollBar::handle:horizontal { min-width: ''' + str(mm2px(6)) + '''px; margin: 0 ''' + str(mm2px(6.2)) + '''px; }
#scene_viewer QScrollBar::sub-line, QScrollBar::add-line { border-radius: ''' + str(mm2px(1)) + '''px; }
#scene_viewer QScrollBar::sub-line:vertical { background: #696 url(:/images/scroll-up.png) center no-repeat; }
#scene_viewer QScrollBar::add-line:vertical { background: #696 url(:/images/scroll-down.png) center no-repeat; }
#scene_viewer QScrollBar::sub-line:horizontal { background: #696 url(:/images/scroll-left.png) center no-repeat; }
#scene_viewer QScrollBar::add-line:horizontal { background: #696 url(:/images/scroll-right.png) center no-repeat; }
*/

/*
QScrollBar { background: #ddd; margin: 0px 0px 0px 0px; }
QScrollBar:vertical { width: ''' + str(mm2px(6)) + '''px; }
QScrollBar:horizontal { height: ''' + str(mm2px(6)) + '''px; }
QScrollBar::handle { border-radius: ''' + str(mm2px(1)) + '''px; background: #696 url(:/images/scroll-grip.png) center no-repeat; }
QScrollBar::handle:verical { min-height: ''' + str(mm2px(6)) + '''px; margin: ''' + str(mm2px(6.2)) + '''px 0; }
QScrollBar::handle:horizontal { min-width: ''' + str(mm2px(6)) + '''px; margin: 0 ''' + str(mm2px(6.2)) + '''px; }
QScrollBar::sub-line, QScrollBar::add-line { border-radius: ''' + str(mm2px(1)) + '''px; }
QScrollBar::sub-line:vertical { background: #696 url(:/images/scroll-up.png) center no-repeat; }
QScrollBar::add-line:vertical { background: #696 url(:/images/scroll-down.png) center no-repeat; }
QScrollBar::sub-line:horizontal { background: #696 url(:/images/scroll-left.png) center no-repeat; }
QScrollBar::add-line:horizontal { background: #696 url(:/images/scroll-right.png) center no-repeat; }
QScrollBar::sub-page, QScrollBar::add-page { background: transparent; }
*/

QTreeWidget { selection-color: white; selection-background-color: #696; show-decoration-selected: 1; }
QTreeWidget::item { padding: ''' + str(mm2px(1.4)) + '''px; }
QTreeWidget::item:focus { }
#tips_widget { background: #cce; }
#tips_image { min-width: ''' + str(mm2px(5)) + '''px; min-height: ''' + str(mm2px(5)) + '''px; max-width: ''' + str(mm2px(5)) + '''px; max-height: ''' + str(mm2px(5)) + '''px; }

/* TAB WIDGET { */
	QTabWidget::pane { border: 0px none transparent; border-bottom: 1px solid #696; }
	QTabWidget::tab-bar { alignment: center; }
	QTabBar::tab { border-top: .6ex solid transparent; border-bottom: .6ex solid transparent; padding: ''' + str(mm2px(1)) + '''px ''' + str(mm2px(2.5)) + '''px; }
	QTabBar::tab:hover { border-top: ''' + str(mm2px(1)) + '''px solid #ccc; }
	QTabBar::tab:selected { border-top: ''' + str(mm2px(1)) + '''px solid #696; }
/* } TAB WIDGET */

/*
#new_checkbox, #bad_checkbox, #fair_checkbox, #good_checkbox { }
QComboBox QAbstractItemView { margin: 5px; border: none 0px transparent; outline: none; padding: 5px; color: #fff; selection-color: #fff; background: #696; selection-background-color: #363; }
QComboBox { border: 1px solid #696; border-radius: 3px; padding: 1px 18px 1px 3px; min-width: 6em; margin: 0px; padding: 0px; min-height: 2.0; background-color: #696; color: #fff; }
QComboBox::drop-down { width: 30px; }
QComboBox QListView QScrollBar { background: rgb(90, 31, 0); color: rgb(253, 231, 146); width: 30px; }
*/

/*
QComboBox { min-height:63px; max-height:63px; margin-right:47px; border-image:url(Resources/ComboBox_Center1.png); font-family:  "Franklin Gothic Medium"; font-size:  22px; }
QComboBox::drop-down { width:47px; border:0px; margin:0px; margin-right:-47px; }
QComboBox::down-arrow { image:url(Resources/ComboBox.png); }
*/

/*
#zoom_fit_button, #zoom_in_button, #zoom_out_button { background-color: rgba(0, 0, 0, 150); border-radius: ''' + str(mm2px(min_touchable_size // 2)) + '''px; }
#zoom_in_button { margin-top: ''' + str(mm2px(1 * min_touchable_size)) + '''px; }
#zoom_out_button { margin-top: ''' + str(mm2px(2 * min_touchable_size)) + '''px; }
*/
#controls_panel QToolButton { border: none 0px transparent; background: rgba(0, 0, 0, 150); border-radius: ''' + str(mm2px(min_touchable_size // 4)) + '''px; }

'''

if __name__ == "__main__":
	print >>sys.stderr, "\nIn order to apply styles for the application insert\n>>> import styles\nright after the construction of the QApplication\n"

else:
	# print [unicode(x) for x in QtWidgets.QStyleFactory.keys()]
	QtWidgets.qApp.setStyle('Fusion')
	QtWidgets.qApp.setStyleSheet(stylesheet)
