#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
from bisect import bisect_left
from colorama import (
	Fore as FG,
	Back as BG,
	Style as ST,
)
import datetime
from gzip import open as gzip_open
import os
import re
import struct
import sys
import threading
import time

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(__file__) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))

# from models.abstract import (
#     Abstract,
# )

# if __name__ == '__main__':
#     from helpers import my_trace; line_profiler = my_trace.start_line_profiler()


class StardictDictionaryMixin(object):

	_filling_threads = []

	# @line_profiler
	def __init__(self, name, ifo_path, idx_path, dict_path):
		self._name = name
		self._ifo_path = ifo_path
		self._idx_path = idx_path
		self._dict_path = dict_path

		self._idx = None
		self._number_size = 4  # Can be 4 or 8
		self._filling_thread = None

		self._filling_thread = threading.Thread(target=self._fill)
		self._filling_thread.setDaemon(True)
		self._filling_thread.start()

	# @line_profiler
	def _fill(self):
		if self.__class__._filling_threads and self.__class__._filling_threads[-1].is_alive():
			self.__class__._filling_threads[-1].join()
		self._filling_threads.append(self._filling_thread)

		t = time.time()
		with open(self._idx_path, 'rb') as index_file:
			self._idx = re.findall(r'([\d\D]+?\x00[\d\D]{%s})' % (self._number_size * 2), index_file.read())
			# self._idx = re.findall(r'([\d\D]+?)\x00([\d\D]{%s})([\d\D]{%s})' % (self._number_size, self._number_size), index_file.read())
		# print >>sys.stderr, datetime.datetime.now().strftime("%H:%M:%S.%f"), '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "StardictDictionaryMixin._fill(), parsing index: time={:.06f}s".format(time.time() - t); sys.stderr.flush()  # FIXME: must be removed

		t = time.time()
		self._lowercase_index = [x.lower() for x in self._idx]
		# self._lowercase_index = map(str.lower, self._idx)
		# print >>sys.stderr, datetime.datetime.now().strftime("%H:%M:%S.%f"), '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "StardictDictionaryMixin._fill(), lowercase: time={:.06f}s".format(time.time() - t); sys.stderr.flush()  # FIXME: must be removed

		# print >>sys.stderr, "self._idx[:3],", self._idx[:3]; sys.stderr.flush()  # FIXME: must be removed
		# print >>sys.stderr, "self._lowercase_index[:3],", self._lowercase_index[:3]; sys.stderr.flush()  # FIXME: must be removed

	# @line_profiler
	def search(self, keywords, limit=1, method='equals', break_if_busy=False, exception_if_busy=False):
		if self._filling_thread is not None and self._filling_thread.is_alive():
			if break_if_busy:
				return []
			elif exception_if_busy:
				raise Exception('Busy')
			else:
				self._filling_thread.join()

		keywords = str(keywords)
		lowercase_keywords = keywords.lower()

		t = time.time()

		_start = bisect_left(self._lowercase_index, lowercase_keywords)
		# print >>sys.stderr, "_start,", _start; sys.stderr.flush()  # FIXME: must be removed

		results = []
		for index in xrange(_start, len(self._idx)):
			_data = self._idx[index]
			# print >>sys.stderr, "_data,", _data; sys.stderr.flush()  # FIXME: must be removed

			_keywords = Keywords(dictionary=self, index=index)

			if _keywords.lower().startswith(lowercase_keywords):
				if method == 'startswith' or method == 'equals' and (keywords == _keywords):
					results.append(_keywords)
					if len(results) >= limit:
						break
			else:
				break

		print >>sys.stderr, "StardictDictionaryMixin.search({}, {}): time={:.06f}s".format(keywords, self._name, time.time() - t); sys.stderr.flush()  # FIXME: must be removed

		return results


class Keywords(str):
	def __new__(cls, dictionary, index):
		return super(Keywords, cls).__new__(cls, dictionary._idx[index][: - (dictionary._number_size * 2 + 1)])

	def __init__(self, dictionary, index):
		self._dictionary = dictionary
		self._idx = index

	# @line_profiler
	def get_article(self):
		t = time.time()

		# return self._dictionary._offsets[self._idx], self._dictionary._sizes[self._idx]
		offset, = struct.unpack_from({4: b'!L', 8: b'!Q'}[self._dictionary._number_size], self._dictionary._idx[self._idx][- (self._dictionary._number_size * 2): - (self._dictionary._number_size)])
		size, = struct.unpack_from({4: b'!L', 8: b'!Q'}[self._dictionary._number_size], self._dictionary._idx[self._idx][- (self._dictionary._number_size):])
		with (gzip_open if self._dictionary._dict_path.endswith('.dz') else open)(self._dictionary._dict_path, 'r') as dictionary_file:
			dictionary_file.seek(offset)
			article = dictionary_file.read(size)

		print >>sys.stderr, "Keywords.get_article(): time={:.06f}s".format(time.time() - t); sys.stderr.flush()  # FIXME: must be removed

		return article


def run_dictionaries_search():
	count = 10
	dictionaries = StardictDictionaryMixin.get_items(from_language='EN', to_language='RU')

	# print >>sys.stderr, "----------------------------"; sys.stderr.flush()  # FIXME: must be removed
	results = [xx for x in [dictionary.search(keywords='Abb', limit=count, method='startswith') for dictionary in dictionaries] for xx in x]
	# results = sorted({xx for x in results for xx in x}, key=str.lower)[:count]
	print >>sys.stderr, "results:", ' | '.join(results); sys.stderr.flush()  # FIXME: must be removed
	if results:
		print >>sys.stderr, "results[0].get_article(),", results[0].get_article(); sys.stderr.flush()  # FIXME: must be removed

	# print >>sys.stderr, "----------------------------"; sys.stderr.flush()  # FIXME: must be removed
	# results = [xx for x in [dictionary.search(keywords='abbrechen', limit=count, method='startswith') for dictionary in dictionaries] for xx in x]
	# # results = sorted({xx for x in results for xx in x}, key=str.lower)[:count]
	# print >>sys.stderr, "results:", ' | '.join(results); sys.stderr.flush()  # FIXME: must be removed
	# if results:
	#     print >>sys.stderr, "results[0].get_article(),", results[0].get_article(); sys.stderr.flush()  # FIXME: must be removed

	print >>sys.stderr, "----------------------------"; sys.stderr.flush()  # FIXME: must be removed
	results = [xx for x in [dictionary.search(keywords='abbrechen', limit=count, method='equals') for dictionary in dictionaries] for xx in x]
	# results = sorted({xx for x in results for xx in x}, key=str.lower)[:count]
	print >>sys.stderr, "results:", ' | '.join(results); sys.stderr.flush()  # FIXME: must be removed
	# if results:
	#     print >>sys.stderr, "results[0].get_article(),", results[0].get_article(); sys.stderr.flush()  # FIXME: must be removed


def main():
	run_dictionaries_search()

if __name__ == '__main__':
	main()

# if __name__ == '__main__':
#     from helpers import my_trace; my_trace.stop_line_profiler(filename_prefix='models/line_profiler_')
