#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import os
import sys

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(__file__) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))

from models.abstract import Memento, ObservableAttrDict


class Settings(ObservableAttrDict):
	def __init__(self):
		self.update(dict(
			# font=None,
			# # font=dict(family='Comic Sans MS', pointSize=10, weight=200, italic=False),
			# # font=dict(family='Droid Sans', pointSize=6, weight=200, italic=False),
			close_on_double_back=True,

			# dict_dictionaries=[],
			# dict_vocabulary=None,
			# dict_font=None,
			# # dict_save_space=True,
			# dict_suggestions_count=20,
		))
		self._memento = memento = Memento('settings.txt')
		self.update(memento.restore(parse_values=True))
		self.changed.bind(memento.save)

	def __repr__(self):
		return object.__repr__(self)


def main():
	pass

if __name__ == '__main__':
	main()
