#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import os
import shutil
import sys
import time

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(__file__) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))

from PyQt import QtSvg

from models.abstract import Memento, ObservableAttrDict, ObservableList, ObservableSet
from models.dictionary import StardictDictionaryMixin


class Data(ObservableAttrDict):
	def __init__(self):
		self.regions = regions = _Regions()
		regions.changed.bind(lambda model=None, previous=None, current=None: (self.changed(self, previous={'regions': previous}, current={'regions': current})))  # invoke parent's changed-event if is changed

		self.chips = chips = _Chips(regions=regions)
		chips.changed.bind(lambda model=None, previous=None, current=None: (self.changed(self, previous={'chips': previous}, current={'chips': current})))  # invoke parent's changed-event if is changed


class _Regions(ObservableSet):
	def __init__(self):
		super(_Regions, self).__init__()

		import images  # Make images be accessed via qt-resources-style (url(":/..."))
		self._renderer = renderer = QtSvg.QSvgRenderer(':/images/map-1.svg')

		for name in ('adn', 'amo', 'and', 'anf', 'ang', 'ano', 'aza', 'bar', 'bay', 'bel', 'beo', 'blu', 'bra', 'bre', 'bro', 'cdu', 'cel', 'cit', 'cmi', 'cod', 'crk', 'dag', 'dal', 'dhw', 'dld', 'dma', 'dol', 'dos', 'dru', 'ech', 'edo', 'eem', 'ekh', 'eli', 'emy', 'ene', 'ent', 'erb', 'esg', 'ett', 'ewa', 'fan', 'far', 'fch', 'fdw', 'fli', 'fnd', 'fra', 'gap', 'gha', 'gla', 'gol', 'gor', 'gun', 'gwa', 'har', 'hde', 'hdw', 'hol', 'hpa', 'hrd', 'hum', 'ice', 'iml', 'iro', 'ise', 'kdu', 'knd', 'lam', 'leb', 'lhu', 'lor', 'los', 'min', 'mmo', 'mti', 'ndo', 'nen', 'nha', 'nig', 'nin', 'nit', 'nrh', 'nrn', 'ofo', 'ofr', 'oro', 'osg', 'por', 'rhu', 'rru', 'sgo', 'shi', 'sit', 'smi', 'son', 'sor', 'sou', 'srh', 'sut', 'tol', 'udu', 'wem', 'wes', 'whe', 'wil', 'wol', 'wwa'):
			region = _Region(name=name, _renderer=renderer)
			region.changed.bind(lambda model=None, previous=None, current=None: (self.changed(self, previous=set(previous), current=set(current))))  # invoke parent's changed-event if is changed
			self.add(region)

	def _find_by_name(self, name):
		return next(x for x in self if x.name == name)


class _Region(ObservableAttrDict):
	def __get_geometry_by_svg_id(self, svg_id):
		if not self._renderer.elementExists(svg_id):
			raise KeyError('Key "{}" not found.'.format(svg_id))
		rect = self._renderer.boundsOnElement(svg_id)
		return rect.x(), rect.y(), rect.width(), rect.height()

	@property
	def geometry(self):
		return self.__get_geometry_by_svg_id(self.name)

	@property
	def free_position(self):
		try:
			x, y, width, height = self.__get_geometry_by_svg_id('{}_place'.format(self.name))
		except KeyError as e:
			x, y, width, height = self.geometry
		x, y = x + .5 * width, y + .5 * height
		return x, y

	def is_adjacent_to(self, region):
		return self._renderer.elementExists('{0}-{1}'.format(self.name, region.name)) \
			or self._renderer.elementExists('{1}-{0}'.format(self.name, region.name))


class _Chips(ObservableSet):
	def __init__(self, regions):
		super(_Chips, self).__init__()

		for chip in (
			_Chip(side='Dwarfs', color='#ee8877', region=regions._find_by_name('erb'), type='army'),
			_Chip(side='Dwarfs', color='#ee8877', region=regions._find_by_name('iro'), type='army'),
			_Chip(side='Dwarfs', color='#ee8877', region=regions._find_by_name('blu'), type='army'),

			_Chip(side='Elves', color='#00dd00', region=regions._find_by_name('iml'), type='army'),
			_Chip(side='Elves', color='#00dd00', region=regions._find_by_name('lor'), type='army'),
			_Chip(side='Elves', color='#00dd00', region=regions._find_by_name('ekh'), type='army'),
			_Chip(side='Elves', color='#00dd00', region=regions._find_by_name('gha'), type='fleet'),
			_Chip(side='Elves', color='#00dd00', region=regions._find_by_name('shi'), type='fellowship'),

			_Chip(side='Gondor', color='#6699ff', region=regions._find_by_name('mti'), type='army'),
			_Chip(side='Gondor', color='#6699ff', region=regions._find_by_name('lam'), type='army'),
			_Chip(side='Gondor', color='#6699ff', region=regions._find_by_name('bel'), type='army'),
			_Chip(side='Gondor', color='#6699ff', region=regions._find_by_name('leb'), type='army'),
			_Chip(side='Gondor', color='#6699ff', region=regions._find_by_name('mti'), type='faramir'),
			_Chip(side='Gondor', color='#6699ff', region=regions._find_by_name('bre'), type='ranger'),
			_Chip(side='Gondor', color='#6699ff', region=regions._find_by_name('cod'), type='gandalf'),

			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('bar'), type='big_army'),
			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('mmo'), type='big_army'),
			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('udu'), type='big_army'),
			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('nrn'), type='big_army'),
			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('dol'), type='army'),
			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('srh'), type='army'),
			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('gun'), type='army'),
			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('bar'), type='sauron'),
			_Chip(side='Mordor', color='#aa44aa', region=regions._find_by_name('dol'), type='nazgul'),  # Any home
			_Chip(side='Mordor', color='#aa44aa', region=None, type='easterling_army'),
			_Chip(side='Mordor', color='#aa44aa', region=None, type='easterling_army'),
			_Chip(side='Mordor', color='#aa44aa', region=None, type='easterling_army'),

			_Chip(side='Rohan', color='#eeee00', region=regions._find_by_name('edo'), type='cavalry'),
			_Chip(side='Rohan', color='#eeee00', region=regions._find_by_name('dhw'), type='cavalry'),
			_Chip(side='Rohan', color='#eeee00', region=regions._find_by_name('hde'), type='army'),

			_Chip(side='Saruman', color='#ddeedd', region=regions._find_by_name('ise'), type='army'),
			_Chip(side='Saruman', color='#ddeedd', region=regions._find_by_name('dld'), type='army'),
			_Chip(side='Saruman', color='#ddeedd', region=regions._find_by_name('kdu'), type='army'),
			_Chip(side='Saruman', color='#ddeedd', region=regions._find_by_name('ise'), type='saruman'),

			_Chip(side='Umbar', color='#00ffdd', region=regions._find_by_name('cit'), type='pirates'),
			_Chip(side='Umbar', color='#00ffdd', region=regions._find_by_name('hrd'), type='army'),
			_Chip(side='Umbar', color='#00ffdd', region=regions._find_by_name('hum'), type='army'),
		):
			chip.changed.bind(lambda model=None, previous=None, current=None: (self.changed(self, previous=set(previous), current=set(current))))  # invoke parent's changed-event if is changed
			self.add(chip)


class _Chip(ObservableAttrDict):
	pass


def main():
	# run_SharedVocabularies()
	pass

if __name__ == '__main__':
	main()
