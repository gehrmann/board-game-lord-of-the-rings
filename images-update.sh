#!/bin/sh

if type 'pyrcc5' 1>/dev/null; then
	pyrcc5 images.qrc > images.py
	sed -i "s/PyQt5/PyQt/" images.py

elif type 'pyrcc4' 1>/dev/null; then
	pyrcc4 images.qrc > images.py
	sed -i "s/PyQt4/PyQt/" images.py

else
	(>&2 echo pyrcc4/pyrcc5 are not found)
fi
