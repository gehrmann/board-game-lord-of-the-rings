#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann


def main():
	import controllers
	controllers.main()

if __name__ == '__main__':
	main()
