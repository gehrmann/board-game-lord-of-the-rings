#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
from colorama import (
	Fore as FG,
)
import datetime
import os
import parse
import re
import ssl
import sys
import threading
import time
import urllib
import urllib2
import urllib3
# import encodings.idna  # Prevent "LookupError: unknown encoding: idna" for urllib3 pool request
import uuid
# import wget
import zipfile

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(__file__) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT

from PyQt import QtCore, QtGui, QtWidgets, uic

from helpers.caller import Caller
from models.abstract import AttrDict, ObservableAttrDict, ObservableSet


class ControllerMixture(object):
	_instance = None

	@classmethod
	def init(cls, **kwargs):
		if cls._instance is None:
			cls._instance = cls(**kwargs)

	""" Model's event handlers """

	""" View's event handlers """

	""" Helpers """

	# @staticmethod
	# def _find_file(directories_paths, filename):
	#     for directory_path in directories_paths:
	#         path = directory_path + filename
	#         if os.path.exists(path):
	#             return path
	#     else:
	#         raise Exception("Can not find file {} in {}".format(filename, directories_paths))

	@staticmethod
	def _load_ui(ui_path):
		import images  # Make images be accessed via qt-resources-style (url(":/..."))

		for path in sys.path:
			if os.path.exists(path + os.sep + ui_path):
				with open(path + os.sep + ui_path) as ui:
					return uic.loadUi(ui)
			elif os.path.isfile(path) and zipfile.is_zipfile(path):
				with zipfile.ZipFile(path) as archive:
					with archive.open(ui_path) as ui:
						return uic.loadUi(ui)
		else:
			raise Exception("Can not find {} in {}".format(ui_path, sys.path))

	@classmethod
	def _show_font_dialog(cls, model, key):
		# dialog = cls._load_ui('views/common_multiwindow_dialog.ui')

		font_dialog = QtWidgets.QFontDialog(*([QtGui.QFont(*[model[key][x] for x in ('family', 'pointSize', 'weight', 'italic')])] if model.get(key, None) is not None else []))
		font_dialog.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)
		# subwindow = dialog.mdiArea.addSubWindow(font_dialog)
		# subwindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
		# subwindow.showMaximized()

		# def on_font_selected(result):
		#     # dialog.close()
		#     setattr(model, key, {
		#         x: value
		#         for x in ('family', 'pointSize', 'weight', 'italic')
		#         for value in [getattr(font_dialog.currentFont(), x)()]
		#     })
		# font_dialog.finished.connect(on_font_selected)

		selected = font_dialog.exec_()
		# dialog.exec_()

		if selected:
			# dialog.close()
			setattr(model, key, {
				x: value
				for x in ('family', 'pointSize', 'weight', 'italic')
				for value in [getattr(font_dialog.currentFont(), x)()]
			})

	@classmethod
	def _show_color_dialog(cls, model, key):
		# dialog = cls._load_ui('views/common_multiwindow_dialog.ui')

		color_dialog = QtWidgets.QColorDialog(*([QtGui.QColor(model[key])] if model.get(key, None) is not None else []))
		color_dialog.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)
		# subwindow = dialog.mdiArea.addSubWindow(color_dialog)
		# subwindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
		# subwindow.showMaximized()

		# def on_color_selected(result):
		#     # dialog.close()
		#     setattr(model, key, unicode(color_dialog.currentColor().name()))
		# color_dialog.finished.connect(on_color_selected)

		selected = color_dialog.exec_()
		# dialog.exec_()

		if selected:
			# dialog.close()
			setattr(model, key, unicode(color_dialog.currentColor().name()))

	@staticmethod
	def _parse_int(value, default):
		try:
			return int(value)
		except ValueError:
			return default

	@staticmethod
	def _convert_previous_data():
		"""
		Check if there is data to convert and show messages how to do it.
		"""

		previous_path = os.path.join(os.getcwd(), '..', 'Vocabulary Learning Game-Box')
		previous_data_path = os.path.realpath(previous_path + os.sep + 'data.db')

		if os.path.exists(previous_path) and os.path.exists(previous_data_path) and not os.path.exists(previous_path + os.sep + 'data.db.converted'):

			QtWidgets.QMessageBox.information(None, 'Congratulations!', 'The App was <b>completely rewritten!</b>')

			result = QtWidgets.QMessageBox.question(None, 'Conversion needed', 'The problem is, that your vocabularies <b>must be converted</b>.', QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Discard, QtWidgets.QMessageBox.Ok)

			if result == QtWidgets.QMessageBox.Discard:
				# Forget about it
				with open(previous_path + os.sep + 'data.db.converted', 'w'):
					pass

			elif result == QtWidgets.QMessageBox.Ok:
				QtWidgets.QMessageBox.information(
					None,
					'How to convert',
					(
						'Email me to <b>{0}</b><br/>'
						'with that file attached:<br/>'
						'<b>{1}</b><br/>'
						'I will send you converted data back.<br/>'
						.format(
							'gehrmann.apps@gmail.com',
							previous_data_path,
						)
					),
				)

	""" Animation """

	# def _animate_color(self, widget, key, from_value, to_value, duration, curve, reset=False):

	#     class ColorAnimator(QtCore.QObject):
	#         def __init__(self, widget, color):
	#             QtCore.QObject.__init__(self)
	#             self._widget = widget
	#             self._color = color
	#             self._previous_stylesheet = widget.styleSheet()

	#         @QtCore.pyqtProperty(QtWidgets.QColor)
	#         def color(self):
	#             return self._color

	#         @color.setter
	#         def color(self, value):
	#             stylesheet = "QLabel { %s: rgba(%d, %d, %d, %d); }" % (key, value.red(), value.green(), value.blue(), value.alpha())
	#             self._widget.setStyleSheet(stylesheet)

	#         def clear_styles(self):
	#             self._widget.setStyleSheet(self._previous_stylesheet)

	#     color_animator = ColorAnimator(widget, QtWidgets.QColor(0, 0, 0, 0))

	#     # print >>sys.stderr, "animate" ; sys.stderr.flush()  # FIXME: must be removed
	#     animation = QtCore.QPropertyAnimation(color_animator, bytes('color'), widget)
	#     animation.setEasingCurve(getattr(QtCore.QEasingCurve, curve))
	#     animation.setDuration(duration)
	#     # animation.setStartValue(0)
	#     # animation.setStartValue(QtWidgets.QColor(100, 100, 100, 255))
	#     animation.setStartValue(QtWidgets.QColor(*from_value) if from_value.__class__ in (tuple, list) else QtWidgets.QColor(from_value))
	#     # animation.setKeyValueAt(.5, QtWidgets.QColor(255, 0, 0, 100))
	#     # animation.setEndValue(10)
	#     animation.setEndValue(QtWidgets.QColor(*to_value) if to_value.__class__ in (tuple, list) else QtWidgets.QColor(to_value))
	#     # animation.setEndValue(QtWidgets.QColor(0, 0, 0, 255))
	#     # animation.setLoopCount(2)
	#     if reset:
	#         animation.finished.connect(color_animator.clear_styles)
	#     animation.start()

	def _animate(self, widget, key, effect=None, from_value=1., to_value=0., duration=1000, count=1, curve='Linear', finished=None):
		"""
		If effect is set as:
			opacity:		key = [ opacity ]
			blur:			key = [ blurRadius ]
			dropShadow:		key = [ blurRadius | color | offset | xOffset | yOffset ]
			colorize:		key = [ color | strength ]
		Returns animation.
		Can not animate two or more different graphic effects.
		Call .start() to animate.
		"""

		if effect is not None:
			effect_name = 'QGraphics' + effect[0].upper() + effect[1:] + 'Effect'
			effect_class = getattr(QtWidgets, effect_name)
			if widget.graphicsEffect().__class__ != effect_class:
				widget.setGraphicsEffect(effect_class())

		animation = QtCore.QPropertyAnimation(widget.graphicsEffect() if effect is not None else widget, bytes(key), widget)
		animation.setEasingCurve(getattr(QtCore.QEasingCurve, curve))
		animation.setStartValue(
			QtCore.QPoint(*from_value) if from_value.__class__ in (tuple, list) and len(from_value) == 2 else (
				QtCore.QRect(*from_value) if from_value.__class__ in (tuple, list) and len(from_value) == 4 else (
					from_value
				)
			)
		)
		animation.setEndValue(
			QtCore.QPoint(*to_value) if to_value.__class__ in (tuple, list) and len(to_value) == 2 else (
				QtCore.QRect(*to_value) if to_value.__class__ in (tuple, list) and len(to_value) == 4 else (
					to_value
				)
			)
		)
		animation.setDuration(duration)
		animation.setLoopCount(count)
		if finished is not None:
			animation.finished.connect(finished)

		return animation

	def _animate_parallel(self, widget, animations):
		""" returns animation group. Call .start() to animate.  """

		animation_group = QtCore.QParallelAnimationGroup(widget)
		for animation in animations:
			animation_group.addAnimation(animation)

		return animation_group

	def _animate_sequential(self, widget, animations):
		""" returns animation group. Call .start() to animate.  """

		animation_group = QtCore.QSequentialAnimationGroup(widget)
		for animation in animations:
			if animation.__class__ is int:
				animation_group.addPause(animation)
			else:
				animation_group.addAnimation(animation)

		return animation_group


class Progress(object):
	def __init__(self, message, thread=None, done=None, total=None):
		self.message, self.thread, self.done, self.total = message, thread, done, total


class ProgressMixture(object):

	def __init__(self):
		self._progress_dialog = None

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		state_model = self.state_model
		view = self._view

		if model is state_model:
			if current is not None and 'progress' in current:
				if state_model.progress is None:
					if self._progress_dialog is None:
						pass
					else:
						self._progress_dialog.close()
						self._progress_dialog = None
				else:
					if self._progress_dialog is None:
						self._progress_dialog = progress_dialog = QtWidgets.QProgressDialog(
							state_model.progress.message if hasattr(state_model.progress, 'message') else state_model.progress,
							None,  # Cancel-button's title
							state_model.progress.done if hasattr(state_model.progress, 'done') else 0,
							state_model.progress.total if hasattr(state_model.progress, 'total') else 0,
							view,
						)
						progress_dialog.setWindowModality(QtCore.Qt.WindowModal)
						progress_dialog.setAutoClose(False)
						progress_dialog.forceShow()
					else:
						self._progress_dialog.setLabelText(state_model.progress.message if hasattr(state_model.progress, 'message') else state_model.progress)
						self._progress_dialog.setValue(state_model.progress.done if hasattr(state_model.progress, 'done') else 0)
						self._progress_dialog.setMaximum(state_model.progress.total if hasattr(state_model.progress, 'total') else 0)
				# progress_dialog.forceShow()
				# for i in range(100):
				#     self._progress_dialog.setValue(i)
				#     time.sleep(1)
				# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "state_model.progress,", state_model.progress; sys.stderr.flush()  # FIXME: must be removed/commented
				# pass
				# if not state_model.progress:
				#     QtWidgets.QToolTip.hideText()
				# else:
				#     # QtWidgets.QToolTip.setFont(QtGui.QFont('Droid Sans', 8))
				#     # QtWidgets.QToolTip.showText(
				#     #     QtCore.QPoint(
				#     #         max(0, (dialog.x() + (dialog.width() - len(state_model.progress) * 6) / 2)),  # Centered
				#     #         dialog.y() + dialog.height() * 5 / 6,  # Almost bottom
				#     #     ),
				#     #     state_model.progress,
				#     # )
				#     # Caller.call_once_after(2., self.__hide_message)  # Re-set a hiding routine. Must be the same function as previously used

	""" View's event handlers """

	""" Helpers """


class TopWidgetMixture(object):
	def __init__(
			self,
			parent_view=None,
			with_dictionaries_button=False,
			with_backgrounds_button=False,
			with_marks_buttons=False,
			with_reload_button=False,
			with_undo_redo_buttons=False,
			with_save_button=False,
	):

		# Models
		""" Current-State-Model for selected view's data"""
		state_model = self.state_model
		state_model.selected_marks = ObservableSet(('new', 'good', 'fair', 'bad'))
		state_model.selected_marks.changed.bind(lambda model=None, previous=None, current=None: (state_model.changed(state_model, previous={'selected_marks': previous}, current={'selected_marks': current})))  # invoke parent's changed-event if is changed
		state_model.new_count = ''
		state_model.bad_count = ''
		state_model.fair_count = ''
		state_model.good_count = ''

		# Views
		self.top_widget = view = self._load_ui('views/top_widget.ui')
		parent_view.addWidget(view)

		view.vocabulary_button.clicked.connect(self.__on_vocabulary_button_clicked)
		if not with_dictionaries_button:
			view.dictionaries_button.setParent(None)
		if not with_backgrounds_button:
			view.backgrounds_button.setParent(None)
		if not with_undo_redo_buttons:
			view.undo_button.setParent(None)
			view.redo_button.setParent(None)
		if not with_marks_buttons:
			view.new_checkbox.setParent(None)
			view.bad_checkbox.setParent(None)
			view.fair_checkbox.setParent(None)
			view.good_checkbox.setParent(None)
		else:
			view.new_checkbox.toggled.connect(self.__on_new_checkbox_toggled)
			view.bad_checkbox.toggled.connect(self.__on_bad_checkbox_toggled)
			view.fair_checkbox.toggled.connect(self.__on_fair_checkbox_toggled)
			view.good_checkbox.toggled.connect(self.__on_good_checkbox_toggled)
		if not with_reload_button:
			view.reload_button.setParent(None)
		if not with_save_button:
			view.save_button.setParent(None)
		view.settings_button.clicked.connect(self._on_settings_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		data_model = self.data_model
		state_model = self.state_model
		view = self.top_widget

		if model is state_model:
			if previous is not None and 'vocabulary' in previous:
				if previous['vocabulary'] is not None:
					previous['vocabulary'].changed.unbind(state_model.changed)  # Unbind from previous vocabulary

			if current is None or 'vocabulary' in current:

				html = ''
				if state_model.vocabulary is None:
					# Show tip: how to select dictinaries
					html += 'Click &nbsp;<img width="{0}" height="{0}" src=":/images/vocabularies-dark.png">&nbsp; to create / select a vocabulary.'.format(int(3 * view.physicalDpiX() / 25.4))
				getattr(view.tips_widget, 'show' if html else 'hide')()
				view.tips_label.setText(html)

				if state_model.vocabulary is not None:
					state_model.vocabulary.changed.bind(state_model.changed)  # Bind to current vocabulary
					state_model.vocabulary.new  # Fix: force pre-loading of the vocabulary in order to prevent needless double-calling of self._fill

				Caller.call_once_after(0, self._fill)

			if current is None or 'selected_marks' in current:
				view.new_checkbox.setChecked('new' in state_model.selected_marks)
				view.good_checkbox.setChecked('good' in state_model.selected_marks)
				view.fair_checkbox.setChecked('fair' in state_model.selected_marks)
				view.bad_checkbox.setChecked('bad' in state_model.selected_marks)

				Caller.call_once_after(0, self._fill)

			if current is None or 'new_count' in current:
				view.new_checkbox.setText(state_model.new_count)

			if current is None or 'bad_count' in current:
				view.bad_checkbox.setText(state_model.bad_count)

			if current is None or 'fair_count' in current:
				view.fair_checkbox.setText(state_model.fair_count)

			if current is None or 'good_count' in current:
				view.good_checkbox.setText(state_model.good_count)

		if model is state_model.vocabulary:
			if current is None or 'new' in current:
				if 'new' in state_model.selected_marks:
					Caller.call_once_after(0, self._fill)

	""" View's event handlers """

	def __on_vocabulary_button_clicked(self, event):
		def on_vocabulary_renamed(item, name):
			item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

		VocabulariesDialog(
			headers=['Vocabulary'],
			data_model=self.data_model.vocabularies,
			model=self.state_model,
			key='vocabulary',
			path_attribute='path',
			path_prefix='vocabularies/',
			is_expanded=True,
			is_container_selectable=False,
			renamed=on_vocabulary_renamed,
			selected=(self._on_vocabulary_selected if hasattr(self, '_on_vocabulary_selected') else None),
		).show()

	def __on_new_checkbox_toggled(self):
		if self.top_widget.new_checkbox.isChecked() != ('new' in self.state_model.selected_marks):
			getattr(self.state_model.selected_marks, 'add' if self.top_widget.new_checkbox.isChecked() else 'remove')('new')

	def __on_bad_checkbox_toggled(self):
		if self.top_widget.bad_checkbox.isChecked() != ('bad' in self.state_model.selected_marks):
			getattr(self.state_model.selected_marks, 'add' if self.top_widget.bad_checkbox.isChecked() else 'remove')('bad')

	def __on_fair_checkbox_toggled(self):
		if self.top_widget.fair_checkbox.isChecked() != ('fair' in self.state_model.selected_marks):
			getattr(self.state_model.selected_marks, 'add' if self.top_widget.fair_checkbox.isChecked() else 'remove')('fair')

	def __on_good_checkbox_toggled(self):
		if self.top_widget.good_checkbox.isChecked() != ('good' in self.state_model.selected_marks):
			getattr(self.state_model.selected_marks, 'add' if self.top_widget.good_checkbox.isChecked() else 'remove')('good')

	""" Helpers """

	def _fill(self):
		state_model = self.state_model
		vocabulary = self.state_model.vocabulary

		if vocabulary is not None:
			state_model.new_count = "{}".format(len(vocabulary.new))
			state_model.bad_count = "{}".format(len(vocabulary.bad))
			state_model.fair_count = "{}".format(len(vocabulary.fair))
			state_model.good_count = "{}".format(len(vocabulary.good))


class CommonTreeDialog(ControllerMixture, ProgressMixture):
	def __init__(
			self,
			parent_view=None,
			headers=None,
			data_model=None,
			model=None,
			key=None,
			selected=None,
			renamed=None,
			preloading=None,
			path_attribute=None,
			path_separator=None,
			path_prefix=None,
			is_expanded=False,
			has_icons=True,
			icons={'item': ':/images/checked.png'},
			has_multiselection=False,
			is_container_selectable=True,
			with_add_button=False,
			with_delete_button=False,
			with_shared_button=False,
			with_download_button=False,
	):
		ProgressMixture.__init__(self)

		self.headers = headers
		self.key = key
		self.path_attribute = path_attribute
		self.path_separator = path_separator
		self.path_prefix = path_prefix
		self.selected = selected  # selected-callback
		self.renamed = renamed  # renamed-callback
		self.preloading = preloading  # preloading-callback
		self.is_expanded = is_expanded
		self.has_icons = has_icons
		self.icons = icons
		self.has_multiselection = has_multiselection
		self.is_container_selectable = is_container_selectable

		# Models
		self.data_model = data_model
		self.model = model
		""" Current-State-Model for selected view's data"""
		self.state_model = state_model = ObservableAttrDict()
		self.state_model.progress = None

		self._lines_to_data = dict()  # Relation between views and models
		self._lines_to_paths = dict()  # Relation between views and models
		self._paths_to_lines = dict()  # Relation between models and views
		self._expanded_paths = set()
		# self._item_to_edit = None

		# Views
		self._dialog = dialog = self._load_ui('views/common_multiwindow_dialog.ui')
		dialog.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)

		self._view = view = self._load_ui('views/common_tree_dialog.ui')
		subwindow = dialog.mdiArea.addSubWindow(view)
		subwindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
		subwindow.showMaximized()

		view.tips_widget.hide()
		if not with_add_button:
			view.add_button.setParent(None)
		else:
			view.add_button.clicked.connect(self.__on_add_button_clicked)
		if not with_delete_button:
			view.delete_button.setParent(None)
		else:
			view.delete_button.clicked.connect(self.__on_delete_button_clicked)
		if not with_shared_button:
			view.shared_button.setParent(None)
		if not with_download_button:
			view.download_button.setParent(None)
		view.tree.itemChanged.connect(self.__on_tree_item_changed)
		view.tree.itemExpanded.connect(self.__on_tree_item_expanded)
		view.tree.itemCollapsed.connect(self.__on_tree_item_collapsed)
		if has_multiselection:
			view.tree.setSelectionMode(QtWidgets.QTreeWidget.MultiSelection)
		view.finished.connect(self.__on_view_finished)
		dialog.closeEvent = self.__on_dialog_closed

		""" Observe models by view """
		if hasattr(data_model, 'changed'):
			data_model.changed.bind(self._on_model_updated)
		state_model.changed.bind(self._on_model_updated, invoke_in_main_thread=True)  # Invoke in main because the progress is running from thread

		""" Fill blank view by models """
		self._on_model_updated(data_model)
		self._on_model_updated(state_model)

	def show(self):
		self._dialog.exec_()

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		ProgressMixture._on_model_updated(self, model=model, previous=previous, current=current)

		data_model = self.data_model

		if model is data_model:
			Caller.call_once_after(0, self.__fill)

	""" View's event handlers """

	def __on_view_finished(self, result):
		self._dialog.close()

	def __on_dialog_closed(self, event=None):
		# values = (self._lines_to_data[x] for x in self._view.tree.selectedItems())
		# value = next(values, None)
		values = [self._lines_to_data[x] for x in self._view.tree.selectedItems()]
		value = values[0] if values else None

		if self.model is not None and self.key is not None:
			setattr(self.model, self.key, (values if self.has_multiselection else value))
		if self.selected is not None:
			self.selected((values if self.has_multiselection else value))

	def __on_tree_item_changed(self, line):
		if line in self._lines_to_data:
			data = self._lines_to_data[line]
			if not line.text(0):  # Restore name if empty
				line.setText(0, self.__get_path(data)[-1])
			else:
				if self.renamed:
					self.renamed(data, unicode(line.text(0)))  # Call renamed-callback

	def __on_tree_item_expanded(self, line):
		path = self._lines_to_paths[line]
		self._expanded_paths.add(path)

		if line in self._lines_to_data:
			data = self._lines_to_data[line]
			if not line.childCount() and self.preloading:
				# self.preloading(data)  # Call preloading-callback
				Caller.call_once_after(.1, self.preloading, data)  # Call preloading-callback

	def __on_tree_item_collapsed(self, line):
		path = self._lines_to_paths[line]
		self._expanded_paths.remove(path)

	def __on_add_button_clicked(self, event):
		# Get path of selected or first top-level directory
		line = self._view.tree.currentItem() or self._view.tree.topLevelItem(0)

		directory_path = self.path_prefix
		try:
			if line not in self._lines_to_paths:  # Get its directory if it is not a directory
				line = line.parent()
			directory_path += self._lines_to_paths[line]
		except KeyError:
			pass
		except AttributeError:
			pass

		if not os.path.isdir(directory_path):
			os.makedirs(directory_path)

		# Create new item
		file_path = os.path.join(directory_path, 'New {}'.format(self.key or 'item'))
		for index in range(9999):
			try:
				item = self.data_model.add(file_path + (' #{}'.format(index) if index else ''))
			except NameError:
				continue
			break
		# self._item_to_edit = item  # Activate edition of its line after refilling tree items

	def __on_delete_button_clicked(self, event):
		# Get selected item
		line = self._view.tree.currentItem()

		if line is None:
			pass

		elif line in self._lines_to_paths:  # Is a directory
			pass  # Not implemented

		else:  # Is a file
			data = self._lines_to_data[line]

			result = QtWidgets.QMessageBox.question(None, 'Confirmation needed', 'Remove focused?', QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel, QtWidgets.QMessageBox.Cancel)
			if result == QtWidgets.QMessageBox.Yes:
				# Remove item
				self.data_model.remove(data)

	""" Helpers """

	def __get_path(self, data):
		path = getattr(data, self.path_attribute) if self.path_attribute is not None else unicode(data)
		if self.path_prefix:
			if path.startswith(self.path_prefix):
				path = path[len(self.path_prefix):]
		return path.split(self.path_separator if self.path_separator is not None else os.sep)

	def __fill(self):
		tree_view = self._view.tree

		if self.has_icons:
			empty_icon = QtGui.QIcon()
			item_icon = QtGui.QIcon(self.icons['item'])

		if self.headers:
			for index, header in enumerate(self.headers):
				tree_view.headerItem().setText(index, header)

		# Re-create tree items
		tree_view.clear()
		self._lines_to_data.clear()
		self._lines_to_paths.clear()
		self._paths_to_lines.clear()
		tree_view.itemChanged.disconnect(self.__on_tree_item_changed)  # Temporarily unbind during bulk-insertion
		for data in self.data_model:
			_path = []
			names = self.__get_path(data)
			directories_names, file_name = names[:-1], names[-1]

			parent_directory_path = ''
			directory_path = ''
			for directory_name in directories_names:
				directory_path += ((self.path_separator if self.path_separator is not None else os.sep) if directory_path else '') + directory_name

				if directory_path not in self._paths_to_lines:
					line = QtWidgets.QTreeWidgetItem([directory_name])
					if self.has_icons:
						line.setIcon(0, empty_icon)
					# if self.has_checkboxes:
					#     line.setCheckState(0, QtCore.Qt.Unchecked)
					#     line.setCheckState(0, QtCore.Qt.Checked if unicode(line.text(0)) in ... else QtCore.Qt.Unchecked)
					line.setChildIndicatorPolicy(QtWidgets.QTreeWidgetItem.ShowIndicator)  # Force showing 'expand'-button
					# line.setExpanded(self.is_expanded)
					# line.setFlags(QtCore.Qt.ItemIsEnabled)
					line.setFlags(line.flags() ^ (0 if self.is_container_selectable else QtCore.Qt.ItemIsSelectable))

					# if file_name and (self.is_expanded or directory_path in self._expanded_paths):
					if self.is_expanded or directory_path in self._expanded_paths:
						self._expanded_paths.add(directory_path)
						# line.setExpanded(True)
						# Caller.call_once_after(0, line.setExpanded, True)

					(self._paths_to_lines[parent_directory_path].addChild if parent_directory_path else tree_view.addTopLevelItem)(line)

					self._paths_to_lines[directory_path] = line
					self._lines_to_paths[line] = directory_path

				parent_directory_path = directory_path

			if directories_names and not file_name:
				self._lines_to_data[line] = data  # Attach data to last directory in hierarchy

			elif file_name:
				line = QtWidgets.QTreeWidgetItem([file_name])
				if self.has_icons:
					line.setIcon(0, item_icon)
				# if self.has_checkboxes:
				#     line.setCheckState(0, QtCore.Qt.Unchecked)
				#     line.setCheckState(0, QtCore.Qt.Checked if unicode(line.text(0)) in ... else QtCore.Qt.Unchecked)
				line.setFlags(line.flags() | (QtCore.Qt.ItemIsEditable if self.renamed is not None else 0))
				(self._paths_to_lines[parent_directory_path].addChild if parent_directory_path else tree_view.addTopLevelItem)(line)

				if self.model is not None and self.key is not None:
					line.setSelected(((data in self.model[self.key]) if self.has_multiselection else (data == self.model[self.key])))  # Select only after addition to parent

				# if self._item_to_edit is not None and self._item_to_edit == data:  # Activate interactive edition of this line
				#     self._item_to_edit = None
				#     tree_view.editItem(line, 0)
				#     # tree_view.openPersistentEditor(line, 0)

				self._lines_to_data[line] = data

		# Re-expand re-created but previously expanded tree containers
		for directory_path in self._expanded_paths:
			if directory_path in self._paths_to_lines:
				line = self._paths_to_lines[directory_path]
				line.setExpanded(True)

		tree_view.sortItems(0, QtCore.Qt.AscendingOrder)
		tree_view.itemChanged.connect(self.__on_tree_item_changed)  # Bind again


class CommonSharedTreeDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(self, *args, with_download_button=True, **kwargs)

		self._view.download_button.clicked.connect(self.__on_download_button_clicked)

	""" Model's event handlers """

	""" View's event handlers """

	def __on_download_button_clicked(self, event):
		# Get selected item
		line = self._view.tree.currentItem()

		if line is None:
			pass

		elif line in self._lines_to_paths:  # Is a directory
			pass  # Not implemented

		else:  # Is a file
			data = self._lines_to_data[line]

			# Download item
			def download():
				for progress in self.data_model.download(data):
					self.state_model.progress = progress
				time.sleep(1.)
				self.state_model.progress = None

			# Caller.call_once_after(.1, download)
			# self.thread = elf.make_breakpoint(threading.Thread(target=send))
			thread = threading.Thread(target=download)
			thread.setDaemon(True)
			thread.start()

	""" Helpers """


class VocabulariesDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(self, *args, with_add_button=True, with_delete_button=True, with_shared_button=True, **kwargs)

		self._view.shared_button.clicked.connect(self.__on_shared_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		super(VocabulariesDialog, self)._on_model_updated(model, previous, current)

		data_model = self.data_model
		view = self._view

		if model is data_model:
			if current is None:  # Only once when view is being filled
				html = ''
				if not data_model:
					# Show tip: how to add vocabularies
					html += (
						'Click &nbsp;<img width="{0}" height="{0}" src=":/images/shared.png">&nbsp; to download from shared source<br/>'
						'<b>or</b> click &nbsp;<img width="{0}" height="{0}" src=":/images/add.png">&nbsp; to create your own'
						.format(int(3 * view.physicalDpiX() / 25.4))
					)
				getattr(view.tips_widget, 'show' if html else 'hide')()
				view.tips_label.setText(html)

	""" View's event handlers """

	def __on_shared_button_clicked(self, event):

		from models.data import _SharedVocabularies
		data_model = _SharedVocabularies()

		Caller.call_once_after(.1, data_model.preload_root)

		SharedVocabulariesDialog(
			headers=['Shared vocabularies'],
			data_model=data_model,
			is_container_selectable=False,
			# has_multiselection=True,
			preloading=lambda item: (data_model.preload_children(item)),
		).show()

		self.data_model.refill()  # Reload the list of local vocabularies

	""" Helpers """


class SharedVocabulariesDialog(CommonSharedTreeDialog):
	pass


class DictionariesDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(self, *args, with_delete_button=True, with_shared_button=True, **kwargs)

		self._view.shared_button.clicked.connect(self.__on_shared_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		super(DictionariesDialog, self)._on_model_updated(model, previous, current)

		data_model = self.data_model
		view = self._view

		if model is data_model:
			if current is None:  # Only once when view is being filled
				html = ''
				if not data_model:
					# Show tip: how to add dictionaries
					html += (
						'Click &nbsp;<img width="{0}" height="{0}" src=":/images/shared.png">&nbsp; to download from shared source<br/>'
						'<b>or</b> copy your own set into:<br/>'
						'<font size="-1">{1}</font>'
						.format(int(3 * view.physicalDpiX() / 25.4), os.getcwd() + os.sep + 'dictionaries')
					)
				getattr(view.tips_widget, 'show' if html else 'hide')()
				view.tips_label.setText(html)

	""" View's event handlers """

	def __on_shared_button_clicked(self, event):

		from models.data import _SharedDictionaries
		data_model = _SharedDictionaries()

		Caller.call_once_after(.2, data_model.preload_root)

		SharedDictionariesDialog(
			headers=['Shared dictionaries'],
			data_model=data_model,
			is_container_selectable=False,
			# has_multiselection=True,
			preloading=lambda item: (data_model.preload_children(item)),
		).show()

		self.data_model.refill()  # Reload the list of local dictionaries

	""" Helpers """


class SharedDictionariesDialog(CommonSharedTreeDialog):
	pass


class BackgroundsCategoriesDialog(CommonTreeDialog):
	def __init__(self, *args, **kwargs):
		CommonTreeDialog.__init__(self, *args, with_delete_button=True, with_shared_button=True, **kwargs)

		self._view.shared_button.clicked.connect(self.__on_shared_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		super(BackgroundsCategoriesDialog, self)._on_model_updated(model, previous, current)

		data_model = self.data_model
		view = self._view

		if model is data_model:
			if current is None:  # Only once when view is being filled
				html = ''
				if not data_model:
					# Show tip: how to add vocabularies
					html += (
						'Click &nbsp;<img width="{0}" height="{0}" src=":/images/shared.png">&nbsp; to download from shared source<br/>'
						'<b>or</b> copy your own set into:<br/>'
						'<font size="-1">{1}</font>'
						.format(int(3 * view.physicalDpiX() / 25.4), os.getcwd() + os.sep + 'backgrounds')
					)
				getattr(view.tips_widget, 'show' if html else 'hide')()
				view.tips_label.setText(html)

	""" View's event handlers """

	def __on_shared_button_clicked(self, event):

		from models.data import _SharedBackgroundsCategories
		data_model = _SharedBackgroundsCategories()

		Caller.call_once_after(.2, data_model.preload_root)

		SharedBackgroundsCategoriesDialog(
			headers=['Shared backgrounds'],
			data_model=data_model,
			is_container_selectable=False,
			# has_multiselection=True,
			preloading=lambda item: (data_model.preload_children(item)),
		).show()

		self.data_model.refill()  # Reload the list of local backgrounds categories

	""" Helpers """


class SharedBackgroundsCategoriesDialog(CommonSharedTreeDialog):
	pass


class CommonSettingsMixture(object):
	def __init__(self):

		# Views
		self.dialog = view = self._load_ui('views/common_settings_dialog.ui')
		view.setWindowFlags(QtCore.Qt.MaximizeUsingFullscreenGeometryHint)

		view.font_button.clicked.connect(self.__on_font_button_clicked)
		view.close_on_double_back_checkbox.stateChanged.connect(self.__on_close_on_double_back_checkbox_changed)
		view.info_button.clicked.connect(self.__on_info_button_clicked)

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		settings_model = self.settings_model
		view = self.dialog

		if model is settings_model:
			if current is None or 'font' in current:
				if settings_model.font is not None:
					font = settings_model.font
					view.window().setStyleSheet("""* {{font: {0[pointSize]}pt "{0[family]}";}}""".format(font) + '\n' + '\n'.join([line for line in unicode(view.window().styleSheet()).split('\n') if not line.startswith('* {font: ')]))
					view.font_button.setText("{0[family]}, {0[pointSize]}{1}{2}".format(font, ', bold' if font['weight'] > 200 else '', ', italic' if font['italic'] else ''))

			if current is None or 'close_on_double_back' in current:
				if settings_model.close_on_double_back is not None:
					view.close_on_double_back_checkbox.setChecked(settings_model.close_on_double_back)

	""" View's event handlers """

	def __on_info_button_clicked(self):
		from controllers.info import InfoController
		InfoController(settings_model=self.settings_model).show()

	def __on_font_button_clicked(self):
		self._show_font_dialog(model=self.settings_model, key='font')

	def __on_close_on_double_back_checkbox_changed(self):
		settings_model = self.settings_model
		settings_model.close_on_double_back = self.dialog.close_on_double_back_checkbox.isChecked()

	""" Helpers """

	def show(self):
		self.dialog.exec_()


class Network(object):
	from urllib3 import exceptions

	# urllib2.install_opener(urllib2.build_opener(urllib2.HTTPHandler(debuglevel=1)))  # urllib2 verbose
	# httplib.HTTPConnection.debuglevel = 1  # urllib3 verbose
	# pool = urllib3.PoolManager(timeout=5., **(dict(ca_certs=cls._find_file(sys.path, '/ca-certificates.crt')) if sys.platform == 'android' else dict()))
	# pool = urllib3.PoolManager(timeout=5.)
	# pool = urllib3.PoolManager(timeout=5., cert_reqs=ssl.CERT_OPTIONAL)
	pool = urllib3.PoolManager(timeout=5., cert_reqs=ssl.CERT_NONE)

	_ga_resource_id = 'UA-42404419-3'

	_previous_session = None
	# _ip = None

	@classmethod
	def send_for_google_analytics(cls, settings_model, session=None, event=None, warning=None, exception=None):
		if cls._ga_resource_id is None:
			raise Exception('Google Analytics resource id is not set.')

		# Resolve client's external IP
		# if cls._ip is None:
		#     s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		#     s.connect(('8.8.8.8', 53))
		#     cls._ip = s.getsockname()[0]
		#     s.close()

		# Description of the protocol: https://developers.google.com/analytics/devguides/vocabulary/protocol/v1/parameters?hl=ru
		query = '&'.join((
			't=' + ('exception' if warning is not None or exception is not None else ('event' if event is not None else 'screenview')),  # [pageview, screenview, event, transaction, item, social, exception, timing]
			('sc=' + session['value']) if session is not None else '',  # session control [, start, end]
			('cd=' + session['key']) if session is not None else '',  # screen location (for screenview)
			('linkid=' + cls._previous_session['key']) if session is not None and cls._previous_session is not None else '',  # previous location
			('ec=' + event['category']) if event is not None else '',  # event category
			('ea=' + event['action']) if event is not None else '',  # event action
			('el=' + event.get('label', '')) if event is not None else '',  # event label
			('ev=' + unicode(event.get('value', 0))) if event is not None else '',  # event value
			'v=1',  # GA-protocol version
			'tid=' + cls._ga_resource_id,
			'ds=application',  # data source, for ex.: web, app, call center
			'cid=' + urllib2.quote(settings_model.setdefault('user-id', lambda: (unicode(uuid.uuid1())))),  # unique client id (from cookies)
			# 'uid=' + urllib2.quote(settings_model.setdefault('user-id', lambda: (unicode(uuid.uuid1())))),  # client name
			# 'uip=' + cls._ip,  # user ip
			'dr=' + urllib2.quote(settings_model.get('referrer', ''), safe=''),  # referrer url
			'cs=' + urllib2.quote(settings_model.get('referrer_source', ''), safe=''),  # referrer source
			'ci=' + urllib2.quote(settings_model.get('referrer_campaign_id', ''), safe=''),  # referrer campaign id
			# 'cn=' + urllib2.quote(settings_model.get('referrer-campaign-name', ''), safe=''),  # referrer campaign name
			# 'cm=' + urllib2.quote(settings_model.get('referrer-campaign-channel', ''), safe=''),  # referrer campaign channel
			'ck=' + urllib2.quote(settings_model.get('referrer_campaign_keywords', ''), safe=''),  # referrer campaign keywords
			# 'cc=' + urllib2.quote(settings_model.get('referrer_campaign_content', ''), safe=''),  # referrer campaign content
			'sr={0}x{1}'.format(QtWidgets.QDesktopWidget().screenGeometry().width(), QtWidgets.QDesktopWidget().screenGeometry().height()),
			'ul=' + unicode(QtCore.QLocale.languageToString(QtCore.QLocale.system().language())),
			'ni=0',  # not interaction
			# 'dl=' + key,  # document location
			# 'dh=',  # document host
			# 'dp=' + key,  # document path
			# 'dt=' + key,  # document title
			'an=' + urllib2.quote(os.environ.get('APPLICATION_NAME', ''), safe=''),
			'aid=' + urllib2.quote(os.environ.get('PACKAGE_NAME', ''), safe=''),
			'av=' + os.environ.get('APPLICATION_VERSION', ''),
			# 'aiid=',  # installer name
			# 'ti=',  # transaction id
			# 'ta=',  # transaction affiliate
			# 'tr=',  # transaction rate
			# ...
			('plt={:d}'.format(int(session['loading_time'] * 1000))) if session is not None and session.get('loading_time', False) else '',  # page loading time (ms)
			# ...
			('exd=' + urllib2.quote(warning or exception, safe='') if warning is not None or exception is not None else ''),  # exception description
			('exf=' + ('0' if warning is not None else '1') if warning is not None or exception is not None else ''),  # exception unrecoverable [0, 1]
			'cd1=' + urllib2.quote(settings_model.setdefault('from_version', os.environ.get('APPLICATION_VERSION', ''))),  # custom parameter 1, max 20
			'cd2=' + urllib2.quote(settings_model.setdefault('theme', 'LightTheme')),  # custom parameter 2
			# 'cm1=',  # custom value 1 [1..200]
			# 'xid=',  # experiment id (always with var)
			# 'xvar=',  # experiment var
		))

		if session is not None:
			cls._previous_session = session

		print >>sys.stderr, datetime.datetime.now().strftime("%H:%M:%S.%f"), FG.CYAN, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), '--- Sending to GA ---', FG.RESET; sys.stderr.flush()  # FIXME: must be removed

		def send():
			time.sleep(.010)  # Write in log a little bit later in order not to mix logs
			try:
				# Check request for errors
				if settings_model.get('DEBUG_GOOGLE_ANALYTICS_REQUESTS', False):
					# response = urllib2.urlopen('http://www.google-analytics.com/debug/collect?' + query)
					# message = response.read()
					response = cls.pool.request('GET', 'http://www.google-analytics.com/debug/collect?' + query)
					message = response.data
					if '"valid": false' in message:
						raise Exception('Wrong GA-Request. Query="{}". Response="{}"'.format(*[x.replace(cls._ga_resource_id, 'xxxxxx') for x in query, message]))

				# Send statistics to GA
				# urllib2.urlopen(urllib2.Request(url='http://www.google-analytics.com/collect?' + query, data=None, headers={'User-agent': os.environ.get('USER_AGENT', urllib2.URLopener.version)} if 'USER_AGENT' in os.environ else {}))
				cls.pool.request('GET', 'http://www.google-analytics.com/collect?' + query)

			except IOError:  # Skip if no connection
				pass

		# self.thread = elf.make_breakpoint(threading.Thread(target=send))
		thread = threading.Thread(target=send)
		thread.setDaemon(True)
		thread.start()

	@classmethod
	def send_postback_to_referrer(cls, settings_model):
		referrer = os.environ.get('INSTALL_REFERRER', '').replace('&equals;', '=')

		if referrer and not settings_model.get('postback_is_sent', False):
			settings_model.postback_is_sent = True

			print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "referrer='{}'".format(referrer); sys.stderr.flush()  # FIXME: must be removed
			settings_model.referrer = referrer
			try:
				source, campaign, keywords, transaction = parse.parse('source={}&campaign={}&keywords={}&transaction={}', referrer)
			except TypeError:
				print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "WARNING: can not parse referrer"; sys.stderr.flush()  # FIXME: must be removed
			else:
				print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "source='{}'".format(source); sys.stderr.flush()  # FIXME: must be removed
				settings_model.referrer_source = source
				settings_model.referrer_campaign_id = campaign
				settings_model.referrer_campaign_keywords = keywords

				url = None
				if source == 'gowide.com':
					url = 'http://track.comboapp.com/aff_lsr?security_token=a947eeb881ab21e8752384fdbde7ba2c&transaction_id={}'.format(transaction)


				# Sh
				shared_referrers_shs = {
					'gowide.com': '0B_7-aHx_p3QTam5FTlhvcGQtU2M',
					'test': '0B_7-aHx_p3QTOGRIWHdrcm9Qd00',
				}

				if source in shared_referrers_shs:
					try:
						# response = urllib2.urlopen(cls._shared_download_url.format(shared_id=shared_referrers_shs[source]))
						# response.read()
						response = cls.pool.request('GET', cls._shared_download_url.format(shared_id=shared_referrers_shs[source]))
						sh = float(response.data)
					except IOError:
						pass
					except ValueError:
						pass
					else:
						import random
						if random.random() > sh:
							url = False


				print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "Postback URL='{}'".format(url); sys.stderr.flush()  # FIXME: must be removed
				if url is not None and url:
					def send():
						try:
							# urllib2.urlopen(url)
							cls.pool.request('GET', url)
							# response = urllib2.urlopen(url)
							# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "response.info(),", response.info(); sys.stderr.flush()  # FIXME: must be removed
						except IOError:
							pass

					# self.thread = elf.make_breakpoint(threading.Thread(target=send))
					thread = threading.Thread(target=send)
					thread.setDaemon(True)
					thread.start()

	@classmethod
	def _send_logs_to_pastebin(cls):
		# import urllib

		with open('native.txt') as f:
			log = f.read()

		post_data = dict(
			api_dev_key='8ade3b45a044fc537f4e59e01d04afa3',
			api_user_key='gehrmann',
			api_option='paste',
			api_paste_private='1',  # 0 - public, 1 - unlisted, 2 - private
			api_paste_format='text',
			api_paste_expire_date='10M',  # 10M - 10 minutes, 1H - 1 hour
			api_paste_name='Logs from ' + datetime.datetime.now().strftime("%H:%M:%S.%f"),
			api_paste_code=log,
		)

		message = None
		try:
			response = cls.pool.request('POST', 'http://pastebin.com/api/api_post.php', fields=post_data)
			message = response.data.replace('http://pastebin.com/', '')
		except Exception as e:
			print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "e,", e; sys.stderr.flush()  # FIXME: must be removed

		return message

	# googledrive.com/host/<page_id>/
	_shared_listing_url = "http://drive.google.com/embeddedfolderview?id={shared_id}"
	_shared_download_url = "http://drive.google.com/uc?id={shared_id}"

	shared_vocabularies_id = "0B_7-aHx_p3QTaUkzaTJyOTNQUWs"
	shared_dictionaries_id = "0B_7-aHx_p3QTUG5vZkl6dEZDUWs"
	shared_backgrounds_categories_id = "0B_7-aHx_p3QTeHpScUhpX1V5ZFE"

	_shared_listing_cache = dict()

	@classmethod
	def get_shared_listing(cls, shared_id, cache_interval=None, raise_exceptions=False):

		# if True:  # Test for SSL-module and send error if something occurs
		#     try:
		#         import _ssl
		#         import ssl
		#     except (ImportError, AttributeError):
		#         sys.excepthook(*sys.exc_info())
		#         if hasattr(sys, 'exception_callback'):
		#             sys.exception_callback()

		if cache_interval is not None and shared_id in cls._shared_listing_cache and cls._shared_listing_cache[shared_id].updated + cache_interval > time.time():
			listing = cls._shared_listing_cache[shared_id].listing
		else:
			try:
				response = Network.pool.request('GET', cls._shared_listing_url.format(shared_id=shared_id))
				response_data = response.data.decode('UTF-8')
			except IOError as e:
				print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "e,", e; sys.stderr.flush()  # FIXME: must be removed
				if raise_exceptions:
					raise
				listing = None
			except (Network.exceptions.ConnectTimeoutError, Network.exceptions.MaxRetryError) as e:
				print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "e,", e; sys.stderr.flush()  # FIXME: must be removed
				if raise_exceptions:
					raise
				listing = None
			else:
				if '<div class="flip-entries">' not in response_data:
					try:
						tmp, response_error, tmp = parse.parse('{}<title>{}</title>{}', response_data)
					except:
						response_error = 'Unknown error'
					print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "response_error,", response_error; sys.stderr.flush()  # FIXME: must be removed
					if raise_exceptions:
						raise Exception(response_error)
					listing = None
				else:
					listing = [
						[x[0], x[1].replace('&lt;', '<').replace('&gt;', '>').replace('&#39;', '\'')]
						for x in re.findall(r'id="entry-([^"]+).*?flip-entry-title">([^<]+)', response_data)
					]
					cls._shared_listing_cache[shared_id] = AttrDict(listing=listing, updated=time.time())
		return listing

	@classmethod
	def download_shared_file(cls, shared_id, dst, autocreate_directories=False, replace=False, skip_exceptions=False):
		src = cls._shared_download_url.format(shared_id=shared_id)

		if dst.__class__ in (tuple, list):
			dst = os.path.join(*dst)

		if replace:
			try:
				os.unlink(dst)
			except OSError:
				pass

		if autocreate_directories:
			try:
				os.makedirs(os.path.join(*os.path.split(dst)[:-1]))
			except OSError:
				pass

		try:
			yield 'Downloading {}'.format(dst)
			# wget.download(url=src, out=dst, bar=lambda *args, **kwargs: (None))  # Fetch
			urllib.urlretrieve(url=src, filename=dst)
			yield 'Done'
		except IOError as e:
			yield 'Exception: {}'.format(e)
			if skip_exceptions:
				print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "e,", e; sys.stderr.flush()  # FIXME: must be removed
			else:
				raise

	@classmethod
	def download_shared_directory(cls, shared_id, dst, autocreate_directories=False, replace=False, skip_exceptions=False):
		items = cls.get_shared_listing(shared_id=shared_id)
		if items is not None:
			try:
				for index, (id, title) in enumerate(items):
					# yield 'Downloading...\n{} of {}'.format(index, len(items))
					yield Progress('Downloading...', thread=threading.current_thread(), done=index, total=len(items))
					for progress in cls.download_shared_file(shared_id=id, dst=dst + os.sep + title, autocreate_directories=autocreate_directories, replace=replace):  # Fetch
						pass
				yield 'Done'
			except IOError as e:
				yield 'Exception: {}'.format(str(e))
				if skip_exceptions:
					print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "e,", e; sys.stderr.flush()  # FIXME: must be removed
				else:
					raise


def run_show_vocabularies_dialog():
	from models.abstract import ObservableAttrDict
	from models.data import _Vocabularies
	# state_model = ObservableAttrDict(
	#     vocabulary=None,
	# )
	data_model = _Vocabularies()

	def on_vocabulary_renamed(item, name):
		item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	VocabulariesDialog(
		headers=['Vocabulary'],
		data_model=data_model,
		path_attribute='path',
		path_prefix='vocabularies/',
		is_expanded=True,
		is_container_selectable=False,
		renamed=on_vocabulary_renamed,
	).show()


def run_show_dictionaries_dialog():
	from models.abstract import ObservableAttrDict
	from models.data import _Dictionaries
	# state_model = ObservableAttrDict(
	#     dictionaries=[],
	# )
	data_model = _Dictionaries()

	def on_dictionary_renamed(item, name):
		item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	DictionariesDialog(
		headers=['Dictionary'],
		data_model=data_model,
		path_attribute='path',
		path_separator='\\',  # Disable splitting by '/'
		path_prefix='dictionaries/',
		is_expanded=True,
		is_container_selectable=False,
		has_multiselection=True,
		# renamed=on_dictionary_renamed,
	).show()


def run_show_backgrounds_categories_dialog():
	from models.abstract import ObservableAttrDict
	from models.data import _BackgroundsCategories
	# state_model = ObservableAttrDict(
	#     backgrounds_categories=[],
	# )
	data_model = _BackgroundsCategories()

	def on_backgrounds_category_renamed(item, name):
		item.path = os.path.join(item.path.rsplit(os.sep, 1)[0], name)

	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	BackgroundsCategoriesDialog(
		headers=['Backgrounds'],
		data_model=data_model,
		path_attribute='path',
		# path_separator='\\',  # Disable splitting by '/'
		path_prefix='backgrounds/',
		is_expanded=True,
		is_container_selectable=False,
		has_multiselection=True,
		# renamed=on_backgrounds_category_renamed,
	).show()


def run_show_shared_vocabularies_dialog():
	# data_model = [
	#     'abc/def',
	#     'xxx/',
	# ]
	from models.data import _SharedVocabularies
	data_model = _SharedVocabularies()

	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	Caller.call_once_after(.1, data_model.preload_root)

	SharedVocabulariesDialog(
		headers=['Shared vocabularies'],
		data_model=data_model,
		is_container_selectable=False,
		# has_multiselection=True,
		preloading=lambda item: (data_model.preload_children(item)),
	).show()


def run_show_shared_dictionaries_dialog():
	# # data_model = [
	# #     'abc/def',
	# #     'xxx/',
	# # ]
	from models.data import _SharedDictionaries
	data_model = _SharedDictionaries()

	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	Caller.call_once_after(.2, data_model.preload_root)

	SharedDictionariesDialog(
		headers=['Shared dictionaries'],
		data_model=data_model,
		is_container_selectable=False,
		# has_multiselection=True,
		preloading=lambda item: (data_model.preload_children(item)),
	).show()


def run_show_shared_backgrounds_categories_dialog():
	# # data_model = [
	# #     'abc/def',
	# #     'xxx/',
	# # ]
	from models.data import _SharedBackgroundsCategories
	data_model = _SharedBackgroundsCategories()

	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	Caller.call_once_after(.2, data_model.preload_root)

	SharedBackgroundsCategoriesDialog(
		headers=['Shared backgrounds'],
		data_model=data_model,
		is_container_selectable=False,
		# has_multiselection=True,
		preloading=lambda item: (data_model.preload_children(item)),
	).show()


def run_progress():
	from models.abstract import ObservableAttrDict
	data_model = ObservableAttrDict()

	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	def _show_progress():
		dialog = QtWidgets.QProgressDialog('Copying', 'Abort copy', 0, 10)
		dialog.setWindowModality(QtCore.Qt.WindowModal)
		dialog.forceShow()
		for i in range(15):
			print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "i,", i; sys.stderr.flush()  # FIXME: must be removed/commented
			dialog.setValue(i)
			time.sleep(1)

	Caller.call_once_after(.2, _show_progress)

	SharedDictionariesDialog(
		headers=['Shared dictionaries'],
		data_model=data_model,
		is_container_selectable=False,
		# has_multiselection=True,
		preloading=lambda item: (data_model.preload_children(item)),
	).show()


def run_balloon():
	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	# QtWidgets.QBalloonTip.balloon()
	QtWidgets.QToolTip.showText(QtCore.QPoint(0, 0), '<font size="+4">test</font><img src="images/bad-on.png"')

	sys.exit(app.exec_())


def run_tray():
	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	# tray = QtWidgets.QSystemTrayIcon(QtGui.QIcon(':/images/dictionaries.png'), app)
	# # tray_menu = QtWidgets.QMenu()
	# # tray_exit_button = tray_menu.addAction("Exit")
	# # tray.setContextMenu(tray_menu)
	# tray.show()
	# tray.showMessage('abc', 'def')

	# QtWidgets.QMessageBox.critical(None, 'abc', 'def <b>ghi</b> <img src=":/images/dictionaries.png" />')

	# dialog = QtWidgets.QDialog()
	# QtCore.pyqtRemoveInputHook(); import ipdb; ipdb.set_trace()  # FIXME: must be removed/commented
	# dialog.show()

	# label = QtWidgets.QLabel('test')
	# label.show()

	sys.exit(app.exec_())


def run_message_logger():
	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	QtCore.QMessageLogger().fatal('test')

	sys.exit(app.exec_())


def run_message_logger():
	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	QtCore.QMessageLogger().fatal('test')

	sys.exit(app.exec_())


def run_send_postback_to_referrer():
	from models.abstract import ObservableAttrDict
	settings_model = ObservableAttrDict()
	os.environ['INSTALL_REFERRER'] = 'source&equals;gowide.com&campaign&equals;gowide_1&keywords&equals;no&transaction&equals;{transaction_id}'

	Network.send_postback_to_referrer(settings_model=settings_model)
	print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "settings_model,", settings_model; sys.stderr.flush()  # FIXME: must be removed/commented


def main():
	# run_show_vocabularies_dialog()
	# run_show_shared_vocabularies_dialog()
	run_show_dictionaries_dialog()
	# run_show_backgrounds_categories_dialog()
	# run_show_shared_dictionaries_dialog()
	# run_show_shared_backgrounds_categories_dialog()
	# run_progress()
	# run_balloon()
	# run_tray()
	# run_message_logger()
	# run_send_postback_to_referrer()

if __name__ == '__main__':
	main()
