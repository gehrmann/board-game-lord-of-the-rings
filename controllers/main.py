#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import datetime
import os
import sys
import time
import traceback

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(__file__) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT

from PyQt import QtCore, QtWidgets, uic

from controllers.abstract import ControllerMixture, Network
# from helpers.my_trace import print_stack
from helpers.caller import Caller
from models.abstract import ObservableAttrDict, ObservableList
from models.data import Data
from models.settings import Settings


class MainController(ControllerMixture):
	def __init__(self):

		# Models
		self.data_model = data_model = Data()
		self.settings_model = settings_model = Settings()

		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), ''.join(['\n\t{} => {}'.format(x, os.environ[x]) for x in sorted(os.environ.keys())]); sys.stderr.flush()  # FIXME: must be removed/commented

		sys.displayhook = lambda etype, value, tb: (
			# print_stack(),
			sys.__displayhook__(etype, value, tb),
			# Network.send_for_google_analytics(settings_model=settings_model, exception=''.join(["ua={{{}}}\n".format(os.environ.get('USER_AGENT', ''))] + traceback.format_exception(etype, value, tb))),
		)  # On exception: send it to Google Analytics

		sys.excepthook = lambda etype, value, tb: (
			# print_stack(),
			sys.__excepthook__(etype, value, tb),
			# Network.send_for_google_analytics(settings_model=settings_model, exception=''.join(["ua={{{}}}\n".format(os.environ.get('USER_AGENT', ''))] + traceback.format_exception(etype, value, tb))),
		)  # On exception: send it to Google Analytics

		QtCore.qInstallMessageHandler(lambda mode, context, message: (
			# print_stack(),
			sys.stderr.write("QT: {0.file}:{0.line}: [{1}] {2}\n".format(context, {QtCore.QtInfoMsg: 'INFO', QtCore.QtWarningMsg: 'WARNING', QtCore.QtCriticalMsg: 'CRITICAL', QtCore.QtFatalMsg: 'FATAL', QtCore.QtDebugMsg: 'DEBUG'}[mode], message)),
			# Network.send_for_google_analytics(settings_model=settings_model, exception="ua={{{}}}\n".format(os.environ.get('USER_AGENT', '')) + "QT: {0.file}:{0.line}: [{1}] {2}\n".format(context, {QtCore.QtInfoMsg: 'INFO', QtCore.QtWarningMsg: 'WARNING', QtCore.QtCriticalMsg: 'CRITICAL', QtCore.QtFatalMsg: 'FATAL', QtCore.QtDebugMsg: 'DEBUG'}[mode], message)),
			None,
		)[-1])  # On qt-exception: send it to Google Analytics

		self._latest_back_pressed_time = 0

		# Views
		self._view = view = self._load_ui('views/main_view.ui')

		self.__restore_window_geometry()
		view.closeEvent = self.__on_close
		view.keyPressEvent = self.__on_key_pressed
		# view.gestureEvent = self.__on_gesture
		# view.grabGesture(QtCore.Qt.PinchGesture)
		# view.viewport().grabGesture(QtCore.Qt.PinchGesture)
		# view.setAttribute(QtCore.Qt.WA_AcceptTouchEvents)
		view.tabs.currentChanged.connect(self.__on_tab_is_changed)
		if settings_model.get('with_speech_tab', False):
			tab_view = QtWidgets.QWidget(parent=view.tabs.widget(0).parent())
			tab_view.setObjectName('speech_tab')
			tab_view.setLayout(QtWidgets.QVBoxLayout())
			tab_view.layout().setContentsMargins(0, 0, 0, 0)
			view.tabs.addTab(tab_view, 'Speech')
			view.speech_tab = tab_view
		view.show()

		# MapController.init(parent_view=view.map_tab.layout(), data_model=data_model)

		""" Observe models by view """
		settings_model.changed.bind(self._on_model_updated)

		""" Fill blank view by models """
		self._on_model_updated(settings_model)

		Caller.call_once_after(.1, self._convert_previous_data)  # Check if there is some data to convert

		Caller.call_once_after(1., Network.send_postback_to_referrer, settings_model=settings_model)

		if '--test-main' in sys.argv:
			import ipdb; ipdb.set_trace()  # FIXME: must be removed

		# self.__start_google_analytics_session_loop()

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		settings_model = self.settings_model
		view = self._view

		if model is settings_model:
			if current is None or 'current_tab' in current:
				view.tabs.setCurrentIndex(settings_model.setdefault('current_tab', 0))
				self.__on_tab_is_changed()

			# if current is None or 'font' in current:
			#     if settings_model.font is not None:
			#         view.setStyleSheet("""* {{font: {0[pointSize]}pt "{0[family]}";}}""".format(settings_model.font) + '\n' + '\n'.join([line for line in unicode(view.styleSheet()).split('\n') if not line.startswith('* {font: ')]))

	""" View's event handlers """

	def __on_key_pressed(self, event):
		if event.key() == QtCore.Qt.Key_Menu:
			current_tab = self._view.tabs.currentWidget()
			getattr(current_tab, 'on_menu_pressed', lambda event: (None))(event)

		if event.key() == QtCore.Qt.Key_Back:
			# Check if back-button is double-clicked
			if self._latest_back_pressed_time + .3 < time.time():
				self._latest_back_pressed_time = time.time()
			else:
				if self.settings_model.close_on_double_back:
					# Exit the application
					QtWidgets.qApp.activeWindow().close()

		if event.key() == QtCore.Qt.Key_Escape:
			self._view.close()

		if event.key() == QtCore.Qt.Key_Pause:
			current_tab = self._view.tabs.currentWidget()
			getattr(current_tab, 'on_paused', lambda event: (None))(event)

	# def __on_gesture(self, event):
	#     print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "event.gesture,", event.gesture; sys.stderr.flush()  # FIXME: must be removed/commented

	def __on_close(self, event=None):
		with open('.geometry', 'w') as storage:
			print >>storage, self._view.saveGeometry()

	def __on_tab_is_changed(self):
		view = self._view
		data_model = self.data_model
		settings_model = self.settings_model
		force_init = False

		settings_model.current_tab = view.tabs.currentIndex()  # Store in settings latest selected tab
		current_tab = view.tabs.currentWidget()

		""" Lazy init of active tab """
		if force_init or current_tab == view.map_tab:
			from controllers.map import MapController
			MapController.init(parent_view=view.map_tab.layout(), data_model=data_model, settings_model=settings_model)

	""" Helpers """

	def __restore_window_geometry(self):
		try:
			with open('.geometry') as storage:
				self._view.restoreGeometry(storage.read())
		except IOError:
			pass

	# def __start_google_analytics_session_loop(self):
	#     # Re-send every 60s a 'session activity' with the name of current tab
	#     def _update_google_analytics_session():
	#         tab_name = unicode(self._view.tabs.currentWidget().objectName())
	#         Network.send_for_google_analytics(settings_model=self.settings_model, session=dict(key=tab_name, value='start'))
	#         Caller.call_once_after(60, _update_google_analytics_session)
	#     _update_google_analytics_session()


def main():
	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	controller = MainController()
	controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
