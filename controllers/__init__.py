# encoding: utf-8
# vim: noexpandtab
# (c) gehrmann

from __future__ import division, unicode_literals
import datetime
import os
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import socket; socket.setdefaulttimeout(10)  # Default timeout for network requests
import sys; reload(sys); sys.setdefaultencoding('utf-8')
import time

from PyQt import QtCore, QtWidgets

enable_profiler = False
# enable_profiler = True

enable_line_profiler = False
# enable_line_profiler = True

if enable_profiler:
	from cProfile import Profile
	profiler = Profile()
	profiler.enable()
	print >>sys.stderr, "PROFILER IS ENABLED"; sys.stderr.flush()

if enable_line_profiler:
	from helpers import my_trace
	sys.line_profiler = my_trace.start_line_profiler()

from controllers.main import MainController


def main():
	app = QtWidgets.QApplication(sys.argv)
	import styles

	MainController.init()
	try:
		sys.exit(app.exec_())
	finally:

		if enable_profiler:
			print >>sys.stderr, "DUMP PROFILE"; sys.stderr.flush()
			profiler.dump_stats('native_profile.prof')

		if enable_line_profiler:
			my_trace.stop_line_profiler(filename_prefix='native_line_profile_')
