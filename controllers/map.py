#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import datetime
import math
import os
import re
import sys
import time
import threading

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(__file__) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))
	import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT

from PyQt import QtCore, QtGui, QtWidgets, uic

from controllers.abstract import ControllerMixture, ProgressMixture
from helpers.caller import Caller
from models.abstract import AttrDict, ObservableAttrDict


class _GestureView(QtWidgets.QGraphicsObject):
	def __init__(self, size, pinch_gestured):
		self._size = size

		super(_GestureView, self).__init__()

		self.grabGesture(QtCore.Qt.PinchGesture)
		self.on_pinch_gestured = pinch_gestured

	""" View's event handlers """

	def paint(self, painter, option, widget):
		pass

	def boundingRect(self):
		return QtCore.QRectF(QtCore.QPointF(0, 0), QtCore.QSizeF(self._size))

	def sceneEvent(self, event):
		if event.type() == QtCore.QEvent.Gesture:
			if event.gesture(QtCore.Qt.PinchGesture) is not None:
				self.on_pinch_gestured(event)
				return True

		return super(_GestureView, self).sceneEvent(event)

	""" Helpers """


class _RegionsView(set):
	def __init__(self, scene, focused=None, unfocused=None):
		self._scene = scene
		self.focused = focused
		self.unfocused = unfocused

	""" View's event handlers """

	def __on_region_view_focused(self, item, event):
		if self.focused is not None:
			self.focused(item, event)

	def __on_region_view_unfocused(self, item, event):
		if self.unfocused is not None:
			self.unfocused(item, event)

	""" Helpers """

	@property
	def model(self):
		return self._model

	@model.setter
	def model(self, regions):
		self._model = regions

		for region_view in self:
			self._scene.removeItem(region_view)

		self.clear()

		for region in regions:
			region_view = _RegionView(model=region, focused=self.__on_region_view_focused, unfocused=self.__on_region_view_unfocused)
			region_view.model = region
			self._scene.addItem(region_view)
			self.add(region_view)

	def hide_all(self):
		for region_view in self:
			region_view.hide()


class _RegionView(QtWidgets.QGraphicsPixmapItem):
	def __init__(self, model, focused=None, unfocused=None):
		self._model = model
		self.focused = focused
		self.unfocused = unfocused

		super(_RegionView, self).__init__(QtGui.QPixmap(':/images/map-1-{}.png'.format(self._model.name)))

		self.setPos(*self._model.geometry[:2])
		self.setFlags(
			0
			| QtWidgets.QGraphicsItem.ItemIsFocusable
			# | QtWidgets.QGraphicsItem.ItemIsSelectable
			# | QtWidgets.QGraphicsItem.ItemIsMovable
			# | QtWidgets.QGraphicsItem.ItemClipsToShape
			# | QtWidgets.QGraphicsItem.ItemSendsScenePositionChanges
		)
		self.setCursor(QtCore.Qt.ArrowCursor)
		# self.setCursor(QtCore.Qt.ForbiddenCursor)
		self.hide()

	""" View's event handlers """

	def focusInEvent(self, event):
		if self.focused is not None:
			self.focused(self, event)
		self.update()

	def focusOutEvent(self, event):
		if self.unfocused is not None:
			self.unfocused(self, event)
		self.update()

	""" Helpers """


class _ChipsView(set):
	def __init__(self, scene, focused=None, unfocused=None):
		self._scene = scene
		self.focused = focused
		self.unfocused = unfocused

	""" View's event handlers """

	def __on_chip_view_focused(self, item, event):
		if self.focused is not None:
			self.focused(item, event)

	def __on_chip_view_unfocused(self, item, event):
		if self.unfocused is not None:
			self.unfocused(item, event)

	""" Helpers """

	@property
	def model(self):
		return self._model

	@model.setter
	def model(self, chips):
		self._model = chips

		for chip_view in self:
			self._scene.removeItem(chip_view)

		self.clear()

		for chip in chips:
			chip_view = _ChipView(model=chip, focused=self.__on_chip_view_focused, unfocused=self.__on_chip_view_unfocused)
			chip_view.model = chip
			self._scene.addItem(chip_view)
			self.add(chip_view)


class _ChipView(object):
	def __new__(cls, model, **kwargs):
		cls = {
			'fellowship': _HeroView,
			'faramir': _HeroView,
			'ranger': _HeroView,
			'gandalf': _HeroView,
			'sauron': _HeroView,
			'nazgul': _HeroView,
			'saruman': _HeroView,
		}.get(model.type, _ArmyView)

		return cls(model, **kwargs)


class _AbstractChipView(QtWidgets.QGraphicsItem):
	_icons = dict()

	_reflex_x = .4
	_reflex_y = .35
	_shadow_x = .1
	_shadow_y = .3
	_shadow_width = .97
	_shadow_height = .8
	_halo_x = -.5
	_halo_y = -.5
	_halo_size = 2.

	def __init__(self, model, focused=None, unfocused=None):
		self._model = model
		self.focused = focused
		self.unfocused = unfocused

		super(_AbstractChipView, self).__init__()

		x, y = self._model.region.free_position if self._model.region is not None else (0, 0)
		self.setX(x - .5 * self._size + getattr(self, '_shift_x', 0))
		self.setY(y - .5 * self._size + getattr(self, '_shift_y', 0))

		self.setFlags(
			0
			| QtWidgets.QGraphicsItem.ItemIsFocusable
			# | QtWidgets.QGraphicsItem.ItemIsSelectable
			# | QtWidgets.QGraphicsItem.ItemIsMovable
			# | QtWidgets.QGraphicsItem.ItemClipsToShape
			# | QtWidgets.QGraphicsItem.ItemSendsScenePositionChanges
		)
		self.setCursor(QtCore.Qt.ArrowCursor)
		# self.setCursor(QtCore.Qt.ForbiddenCursor)

	""" View's event handlers """

	def boundingRect(self):
		halo_x, halo_y, halo_size = self._halo_x, self._halo_y, self._halo_size

		return QtCore.QRectF(min(0, halo_x * self._size), min(0, halo_y * self._size), max(self._size, halo_size * self._size), max(self._size, halo_size * self._size))

	def shape(self):
		shape = QtGui.QPainterPath()
		shape.addEllipse(0, 0, self._size, self._size)
		return shape

	def focusInEvent(self, event):
		if self.focused is not None:
			self.focused(self, event)
		self.update()

	def focusOutEvent(self, event):
		if self.unfocused is not None:
			self.unfocused(self, event)
		self.update()

	""" Helpers """

	@classmethod
	def _get_icon(cls, path):
		if path not in cls._icons:
			cls._icons[path] = QtGui.QIcon(path)

		return cls._icons[path]

	@property
	def _icon(self):
		path = {
			'army': ':/images/helmet.svg',
			'fleet': ':/images/fleet.svg',
			'big_army': ':/images/helmet.svg',
			'cavalry': ':/images/horseshoe.svg',
			'pirates': ':/images/pirates.svg',
		}.get(self._model.type, '')

		return self._get_icon(path)

	@property
	def _size(self):
		return 60 if self._model.type == 'big_army' else (25 if self.__class__ == _HeroView else 40)

	@property
	def _color(self):
		return QtGui.QColor(self._model.color)


class _ArmyView(_AbstractChipView):

	""" View's event handlers """

	def paint(self, painter, option, widget):
		shadow_x, shadow_y, shadow_width, shadow_height = self._shadow_x, self._shadow_y, self._shadow_width, self._shadow_height
		halo_x, halo_y, halo_size = self._halo_x, self._halo_y, self._halo_size
		reflex_x, reflex_y = self._reflex_x, self._reflex_y

		painter.setRenderHint(painter.Antialiasing, True)
		painter.setPen(QtCore.Qt.NoPen)
		# painter.setPen(QtGui.QPen(QtGui.QColor('#666666'), 0))

		# Shadow
		painter.setBrush(QtGui.QColor(0, 0, 0, 100))
		painter.drawEllipse(shadow_x * self._size, shadow_y * self._size, shadow_width * self._size, shadow_height * self._size)

		# Halo
		if self == self.focusItem():
			gradient = QtGui.QRadialGradient(.25 * halo_size * self._size, .25 * halo_size * self._size, .5 * halo_size * self._size)
			gradient.setColorAt(0., self._color.darker(120))
			gradient.setColorAt(1., QtGui.QColor(255, 255, 255, 0))
			painter.setBrush(gradient)
			painter.drawEllipse(halo_x * self._size, halo_y * self._size, halo_size * self._size, halo_size * self._size)

		# Ball
		gradient = QtGui.QRadialGradient(reflex_x * self._size, reflex_y * self._size, self._size)
		gradient.setColorAt(.0, self._color.lighter(200))
		gradient.setColorAt(.3, self._color)
		gradient.setColorAt(.6, self._color.darker(200))
		painter.setBrush(QtGui.QBrush(gradient))
		painter.drawEllipse(0, 0, self._size, self._size)

		# Icon
		painter.drawPixmap(.2 * self._size, .2 * self._size, .6 * self._size, .6 * self._size, self._icon.pixmap(120, 120, QtGui.QIcon.Normal))


class _HeroView(_AbstractChipView):
	_latest_hero_index = 0

	def __init__(self, *args, **kwargs):
		self._hero_index = self.__generate_hero_index()
		# self._model.type = str(self._hero_index) + self._model.type
		self._shift_x = 23 * math.sin(- .28 * math.pi * self._hero_index)
		self._shift_y = 23 * math.cos(- .28 * math.pi * self._hero_index)

		super(_HeroView, self).__init__(*args, **kwargs)

	""" View's event handlers """

	def paint(self, painter, option, widget):
		shadow_x, shadow_y, shadow_width, shadow_height = self._shadow_x, self._shadow_y, self._shadow_width, self._shadow_height
		halo_x, halo_y, halo_size = self._halo_x, self._halo_y, self._halo_size
		reflex_x, reflex_y = self._reflex_x, self._reflex_y

		painter.setRenderHint(painter.Antialiasing, True)
		painter.setPen(QtCore.Qt.NoPen)
		# painter.setPen(QtGui.QPen(QtGui.QColor('#666666'), 0))

		# Halo
		if self == self.focusItem():
			gradient = QtGui.QRadialGradient(.25 * halo_size * self._size, .25 * halo_size * self._size, .5 * halo_size * self._size)
			gradient.setColorAt(0., self._color.darker(120))
			gradient.setColorAt(1., QtGui.QColor(255, 255, 255, 0))
			painter.setBrush(gradient)
			painter.drawEllipse(halo_x * self._size, halo_y * self._size, halo_size * self._size, halo_size * self._size)

		# Ball
		gradient = QtGui.QRadialGradient(.5 * self._size, .5 * self._size, self._size)
		gradient.setColorAt(.0, self._color)
		gradient.setColorAt(.6, QtGui.QColor(255, 255, 255, 0))
		painter.setBrush(QtGui.QBrush(gradient))
		painter.drawEllipse(0, 0, self._size, self._size)

		# Name
		font = QtGui.QFont()
		font.setBold(True)
		font.setPixelSize(12)
		font.setFamily('Serif')
		painter.setFont(font)
		painter.setPen(QtGui.QPen(QtGui.QColor('#000000'), 0))
		painter.drawText(.34 * self._size, .1 * self._size + painter.fontMetrics().height(), self._model.type[0].upper())

	""" Helpers """

	@classmethod
	def __generate_hero_index(cls):
		cls._latest_hero_index += 1
		return cls._latest_hero_index


class _State(ObservableAttrDict):
	pass


class MapController(ControllerMixture, ProgressMixture):
	def __init__(self, parent_view=None, test=False, data_model=None, settings_model=None):
		t1 = time.time()

		ProgressMixture.__init__(self)

		# Models
		""" Current-State-Model for selected view's data"""
		self.state_model = state_model = _State()
		state_model.selected_chip = None
		state_model.selected_region = None

		self.data_model = data_model

		self.settings_model = settings_model

		# Touch-devices are only on PyQt5 available
		touch_screen_devices = [device for device in (QtGui.QTouchDevice.devices() if hasattr(QtGui, 'QTouchDevice') else []) if device.type() == device.TouchScreen]

		# Views
		self._view = view = self._load_ui('views/map_view.ui')
		if parent_view is not None:
			parent_view.addWidget(view)
			parent_view.parent().on_paused = self.__on_paused  # Main controller is looking for a handler on the selected tab
		view.change_map_button.clicked.connect(self.__on_change_map_button_clicked)
		view.zoom_fit_button.clicked.connect(self.__on_zoom_fit_button_clicked)
		if not touch_screen_devices:
			view.zoom_in_button.clicked.connect(self.__on_zoom_in_button_clicked)
			view.zoom_out_button.clicked.connect(self.__on_zoom_out_button_clicked)
		else:
			view.zoom_in_button.hide()
			view.zoom_out_button.hide()
		view.attack_button.clicked.connect(self.__on_attack_button_clicked)
		view.support_button.clicked.connect(self.__on_support_button_clicked)
		view.scene_viewer.resizeEvent = (lambda previous_callback: (lambda event: (self.__on_screen_viewer_resized(event, previous_callback))))(view.scene_viewer.resizeEvent)
		# view.scene_viewer.mousePressEvent = (lambda previous_callback: (lambda event: (self.__on_scene_viewer_mouse_pressed(event, previous_callback))))(view.scene_viewer.mousePressEvent)
		if 1:  # Logical shift for sub-widgets
			self.scene = scene = QtWidgets.QGraphicsScene()
			if 1:  # Logical shift for sub-widgets

				self.map_image = map_image = QtGui.QPixmap(':/images/map-1.jpg')
				self.map_widget = map_widget = scene.addPixmap(map_image)
				# map_widget.hide()

				self.shadowed_map_image = shadowed_map_image = QtGui.QPixmap(':/images/map-1b.jpg')
				self.shadowed_map_widget = shadowed_map_widget = scene.addPixmap(shadowed_map_image)
				shadowed_map_widget.hide()

				self.gesture_view = gesture_view = _GestureView(size=map_image.size(), pinch_gestured=self.__on_pinch_gestured)
				scene.addItem(gesture_view)

				self.regions_view = regions_view = _RegionsView(scene, focused=self.__on_region_view_focused, unfocused=self.__on_region_view_unfocused)

				self.chips_view = chips_view = _ChipsView(scene, focused=self.__on_chip_view_focused, unfocused=self.__on_chip_view_unfocused)

			view.scene_viewer.setScene(scene)

			# Caller.call_once_after(.01, self.__fit_to_screen)

		if parent_view is None:
			view.show()

		""" Observe models by view """
		state_model.changed.bind(self._on_model_updated, invoke_in_main_thread=True)  # Invoke in main because the progress is running from thread
		data_model.changed.bind(self._on_model_updated)
		settings_model.changed.bind(self._on_model_updated)

		""" Fill blank view by models """
		self._on_model_updated(state_model)
		self._on_model_updated(data_model)
		self._on_model_updated(settings_model)

		if '--test-map' in sys.argv:
			import ipdb; ipdb.set_trace()  # FIXME: must be removed

		t2 = time.time()
		print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "Loaded in {}s.".format(t2 - t1); sys.stderr.flush()  # FIXME: must be removed/commented

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed

		ProgressMixture._on_model_updated(self, model=model, previous=previous, current=current)

		state_model = self.state_model
		data_model = self.data_model
		settings_model = self.settings_model
		view = self._view

		if model is state_model:
			if current is None or 'selected_chip' in current:
				if state_model.selected_chip is not None:
					self.map_widget.hide()
					self.shadowed_map_widget.show()
					self.regions_view.hide_all()
					# Show only adjacent regions
					for region_view in self.regions_view:
						if region_view._model.is_adjacent_to(state_model.selected_chip._model.region):
							region_view.show()
				else:
					self.map_widget.show()
					self.shadowed_map_widget.hide()
					self.regions_view.hide_all()
			if current is None or 'selected_region' in current:
				if state_model.selected_region is not None:
					Caller.call_once_after(.01, self.__show_action)
				else:
					pass

		if model is data_model:
			if current is None or 'regions' in current:
				self.regions_view.model = data_model.regions

			if current is None or 'chips' in current:
				self.chips_view.model = data_model.chips

	""" View's event handlers """

	def __on_paused(self, event):
		view = self._view
		scene = view.scene_viewer

		QtCore.pyqtRemoveInputHook(); import ipdb; ipdb.set_trace()  # FIXME: must be removed/commented

	def __on_zoom_in_button_clicked(self, checked):
		self._scale_map(1.2)

	def __on_zoom_out_button_clicked(self, checked):
		self._scale_map(.8)

	def __on_change_map_button_clicked(self, checked):
		self.map_widget.setVisible(not self.map_widget.isVisible())
		self.shadowed_map_widget.setVisible(not self.shadowed_map_widget.isVisible())

		# for widget in self.regions_widgets.values():
		#     widget.setVisible(not widget.isVisible())

		# map_widget = self.map_widget

		# map_widget.path = ':/images/map-{}.jpg'.format(2 if '1' in map_widget.path else 1)

		# self.scene.update()

	def __on_zoom_fit_button_clicked(self, checked):
		self.__fit_to_screen()

	def __on_pinch_gestured(self, event):
		scene_viewer = self._view.scene_viewer

		pinch = event.gesture(QtCore.Qt.PinchGesture)
		self._scale_map(pinch.scaleFactor())

		# Break mouse events into two separate movements around gesture in order to avoid jump of the map
		if scene_viewer.mouseMoveEvent.__name__ == 'mouseMoveEvent':
			scene_viewer.mouseMoveEvent = (lambda previous_callback: (lambda event: (self.__break_next_mouse_move_event(event, previous_callback))))(scene_viewer.mouseMoveEvent)

	def __on_screen_viewer_resized(self, event, previous_callback):
		view = self._view
		import styles

		view.controls_panel.setGeometry(
			view.window().width() - styles.mm2px(styles.min_touchable_size * 1.25),
			styles.mm2px(styles.min_touchable_size * .25),
			styles.mm2px(styles.min_touchable_size),
			styles.mm2px(styles.min_touchable_size * (len([child for child in view.controls_panel.children() if isinstance(child, QtWidgets.QToolButton) and not child.isHidden()]) + 0.25 + 1)),
		)

		return previous_callback(event)

	def __on_chip_view_focused(self, item, event):
		print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "chip focused" ; sys.stderr.flush()  # FIXME: must be removed/commented
		self.state_model.selected_chip = item

	def __on_chip_view_unfocused(self, item, event):
		print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "chip unfocused" ; sys.stderr.flush()  # FIXME: must be removed/commented
		# self.state_model.selected_chip = None
		pass

	def __on_region_view_focused(self, item, event):
		print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "region focused" ; sys.stderr.flush()  # FIXME: must be removed/commented
		self.state_model.selected_region = item

	def __on_region_view_unfocused(self, item, event):
		print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "region unfocused" ; sys.stderr.flush()  # FIXME: must be removed/commented
		# self.state_model.selected_region = None
		pass

	# def __on_scene_viewer_mouse_pressed(self, event, previous_callback):
	#     view = self._view
	#     item = view.scene_viewer.itemAt(event.x(), event.y())
	#     print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), 'mouse pressed on', item.__class__.__name__, item.zValue(); sys.stderr.flush()  # FIXME: must be removed/commented
	#     return previous_callback(event)

	def __on_attack_button_clicked(self, checked):
		print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "attack" ; sys.stderr.flush()  # FIXME: must be removed/commented

	def __on_support_button_clicked(self, checked):
		print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "support" ; sys.stderr.flush()  # FIXME: must be removed/commented

	""" Helpers """

	def __break_next_mouse_move_event(self, event, previous_callback):
		""" Breaks mouse events into two separate movements. Avoids jump after gesture. """

		scene_viewer = self._view.scene_viewer

		scene_viewer.mouseReleaseEvent(event)
		scene_viewer.mouseMoveEvent = previous_callback
		scene_viewer.mouseMoveEvent(event)
		print '',  # A small fix to force syscall?

	def _scale_map(self, scale):
		self._view.scene_viewer.scale(scale, scale)

	def __fit_to_screen(self):
		# self._view.scene_viewer.fitInView(self.map_widget, mode=QtCore.Qt.KeepAspectRatio)
		self._view.scene_viewer.fitInView(self.map_widget, mode=QtCore.Qt.KeepAspectRatioByExpanding)

	def __show_action(self):
		state_model = self.state_model

		state_model.selected_chip._model.region = state_model.selected_region._model

		state_model.selected_chip = None
		state_model.selected_region = None


def run():
	from models.abstract import ObservableAttrDict
	data_model = ObservableAttrDict(
	)
	settings_model = ObservableAttrDict(
	)

	app = QtWidgets.QApplication(sys.argv)
	import styles  # Load once the styles for QApplication

	controller = MapController(data_model=data_model, settings_model=settings_model)
	controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def main():
	run()

if __name__ == '__main__':
	main()
