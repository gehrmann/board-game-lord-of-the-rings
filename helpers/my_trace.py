#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

"""

Breakpoint mode:

F4 - enable breakpoints
n - to next breakpoint
c - disable breakpoints

Example:
	import my_trace; my_trace.include(
		'IsSelected',
		'GetCurrentImage',
		'HitTest',
		'OnMouse',
	)  # FIXME: must be removed
	import my_trace; my_trace.breakpoints(
		'GetCurrentImage',
	)  # FIXME: must be removed
	import my_trace; my_trace.tracer_enable()  # FIXME: must be removed

"""

import linecache
import os
import sys
import threading
import time

try:
	import IPython
except Exception:
	pass


_is_debugger_enabled = False
_debugger_filename = ""
_debugger_lineno = ""
_debugger_function = ""
_debugger_shell = None
_debugger_type = "console"

# _is_debugger_enabled = True
# _debugger_filename = "my_trace"
# _debugger_lineno = "610"
# # _debugger_function = "loop"
# _debugger_type = "console"

_is_tracer_enabled = False
_tracer_line_by_line = False
_tracer_include = []
_tracer_exclude = []
_tracer_sleep_event = threading.Event()
_tracer_sleep = 0
_tracer_is_paused = False
_tracer_breakpoints = []
_tracer_breakpoints_are_enabled = False
_tracer_offset = 0
_tracer_previous_timestamp = time.time()
_tracer_prefix = os.getcwd() + "/"

# _current_path = os.getcwd() + '/'
_home_path = os.path.expanduser('~')
if _home_path.startswith('/home/'):
	_home_path = '/'.join(_home_path.split('/')[0:3])


# def trace(_newLine = False, __previous_timestamp=[None]):
#     global _is_tracer_enabled
#     global _tracer_offset

#     if _is_tracer_enabled:
#         if not __previous_timestamp[0]:
#             __previous_timestamp[0] = time.time()

#         try:
#             for level in range(sys.getrecursionlimit()):
#                 sys._getframe(level)
#         except ValueError:
#             pass

#         from_frame = inspect.getframeinfo(sys._getframe(1))

#         print "" \
#             + ("\n" if _newLine else "") \
#             + ("+%.6f\t" % (time.time() - __previous_timestamp[0])) \
#             + "│ " * (level + _tracer_offset) \
#             + "┡━┓" \
#             + from_frame.function \
#             + "()" \
#             + "  " \
#             + from_frame.filename.rsplit('/', 1)[-1].split('.', 1)[0] \
#             + ":" \
#             + str(from_frame.lineno) \
#             + ""
#         sys.stderr.flush()

#         __previous_timestamp[0] = time.time()


def dump_object(item, depth=1, __keys=None, __cache=None, __level=0):
	import sys

	if __cache is None:
		__cache = set()
	if __keys is None:
		__keys = []
	tab = "        "

	if type(item) in (str, unicode, int, float):
		print \
			("u" if isinstance(item, unicode) else "") + \
			('"' + str(item).replace("\n", "\\n") + '"' if isinstance(item, (str, unicode)) else str(item))
	else:
		print object.__repr__(item).strip('<>').rsplit(' object at ')[0]
		# print object.__repr__(item)

	if hasattr(item, '__dict__') and item.__dict__ or hasattr(item, '__iter__'):
		if depth > __level:
			__cache.add(id(item))

			if isinstance(item, dict) and item or hasattr(item, '__dict__') and item.__dict__:
				for key, value in (item if isinstance(item, dict) else item.__dict__).items():
					sys.stderr.write(tab * (__level) + tab)

					print ".{:} =>".format(key),
					dump_object(value, depth=depth, __keys=(key, ), __cache=__cache, __level=__level + 1)

			if hasattr(item, '__iter__') and type(item) != type and not isinstance(item, dict):
				sys.stderr.write(tab * (__level) + tab + "(\n")

				if item:
					for index, value in enumerate(item):
						if index > 0:
							sys.stderr.write(tab * (__level) + tab + "), (\n")

						sys.stderr.write(tab * (__level + 1) + "    ")

						dump_object(value, depth=depth, __keys=__keys, __cache=__cache, __level=__level+1)

				sys.stderr.write(tab * (__level) + tab + ")\n")

	sys.stderr.flush()


event_types = None


def trace_wx_events(item, exclude=()):
	global event_types

	if event_types is None:
		import wx
		event_types = {}
		for key in dir(wx):
			if key.startswith('EVT_'):
				event = getattr(wx, key)
				if isinstance(event, wx.PyEventBinder):
					for type_ in event.evtType:
						event_types[type_] = key

	def _on_event(app, event):
		if event.GetEventObject() == item:

			try:
				event_name = event_types[event.GetEventType()]
			except KeyError:
				event_name = type(event),

			if event_name not in exclude:
				# (
				#     'EVT_IDLE',
				#     'EVT_UPDATE_UI_RANGE',
				# ):
				print event_name,
				sys.stderr.flush()

	app = wx.GetApp()
	app.__class__.FilterEvent = _on_event
	app.SetCallFilterEvent(True)


def print_stack():
	try:
		for level in range(sys.getrecursionlimit()):
			sys._getframe(level)
	except ValueError:
		pass

	for i in range(level - 1, -1, -1):
		from_frame = sys._getframe(i)
		trace_callback(from_frame, 'call', (), _force_enable=True)


def trace_callback(to_frame, event, arg, _force_enable=False):
	# print "event:", event; sys.stderr.flush()  # FIXME: must be removed
	# event == 'line'
	debug_callback = False
	global _is_debugger_enabled

	# print ".", ; sys.stderr.flush()  # FIXME: must be removed
	if _is_debugger_enabled:
		global _debugger_shell

		from_frame = to_frame.f_back if to_frame.f_back is not None else to_frame

		from_path = from_frame.f_code.co_filename
		from_filename = from_path.rsplit('/', 1)[-1].split('.', 1)[0]
		# from_function = from_frame.f_code.co_name
		from_lineno = from_frame.f_lineno

		to_path = to_frame.f_code.co_filename
		to_filename = to_path.rsplit('/', 1)[-1].split('.', 1)[0]
		to_function = to_frame.f_code.co_name
		to_lineno = str(to_frame.f_lineno)

		# Test for trace
		if 0 and to_filename.startswith("my_trace"):
			print >>sys.stderr, "{:>10s} {:}:{:} ({:}) with {:}:{:} ({:})".format(
				event,
				to_filename,
				to_lineno,
				to_function,
				_debugger_filename,
				_debugger_lineno,
				_debugger_function,
			)
			sys.stderr.flush()

		if \
				event == "call" and to_function == _debugger_function or \
				to_filename == _debugger_filename and to_lineno == _debugger_lineno:

			def _refresh():
				import wx
				app = wx.GetApp()
				wx.CallAfter(app.ExitMainLoop)
				app.MainLoop()

			if _debugger_type in ("ipython", "console", "c"):
				"""IPython Console

				+++ supports local/global variables
				+++ exit without confirmation
				+/- console only
				"""

				# IPython.embed()
				if _debugger_shell is None:
					config = IPython.config.loader.Config()
					config.InteractiveShell.confirm_exit = False
					_debugger_shell = IPython.frontend.terminal.embed.InteractiveShellEmbed(
						config=config,
						banner1="\n" + "-"*40 + "\nEnter to IPython interactive console:",
						display_banner=True,
						exit_msg="Exit from IPython interactive console\n" + "-"*40,
						user_ns={
							'refresh': _refresh,
							'loop': _refresh,
						},
						# ipython_dir=None, user_module=None, custom_exceptions=((), None),
						# banner2=None, user_global_ns=None
					)

				_debugger_shell(
					# header='', module=None, dummy=None, stack_depth=1
					global_ns=to_frame.f_globals,
					local_ns=to_frame.f_locals.update({
						# 1: local_var,
					}),
				)

			elif _debugger_type in ("ipdb", "debugger", "d"):
				"""IPython Debugger

				--- destroys tracing
				--- doesn't support locals/globals
				+/- console only
				"""

				def _trace(*args):
					IPython.core.debugger.Tracer(
						colors=IPython.frontend.terminal.embed.InteractiveShellEmbed().colors
					).debugger.set_trace(to_frame)
				debug_callback = _trace

			elif _debugger_type in ("pycrust", "crust", "wx", "w", "gui", "g"):
				"""WX PyCrust Console

				+/- doesn't lock the execution
				+++ supports local/global variables
				+/- GUI only
				"""

				import my_shell
				my_shell.show()
				my_shell.set_globals(to_frame.f_globals)
				my_shell.set_locals(to_frame.f_locals)

			# _is_debugger_enabled = False

	if _is_tracer_enabled or _force_enable:
		if _tracer_line_by_line or event == "call":
			global _tracer_is_paused
			global _tracer_breakpoints_are_enabled
			global _tracer_previous_timestamp
			global _tracer_prefix

			from_frame = to_frame.f_back if to_frame.f_back is not None else to_frame
			from_path = from_frame.f_code.co_filename
			to_path = to_frame.f_code.co_filename
			# from_function = from_frame.f_code.co_name
			to_function = to_frame.f_code.co_name
			from_lineno = from_frame.f_lineno
			to_lineno = str(to_frame.f_lineno)

			# Make path absolute
			# if not from_path.startswith('/'):
			#     from_path = _current_path + from_path
			# if not to_path.startswith('/'):
			#     to_path = _current_path + to_path

			# Replace home-part of path with tilde
			if from_path.startswith(_home_path):
				from_path = '~' + from_path[len(_home_path):]
			if to_path.startswith(_home_path):
				to_path = '~' + to_path[len(_home_path):]

			from_filename = from_path.rsplit('/', 1)[-1]
			to_filename = to_path.rsplit('/', 1)[-1]

			# Filter by white-list
			if _tracer_include and all(all((
					# path not in from_path,
					# path not in from_lineno,
					# path not in from_function,
					path not in to_path,
					path not in to_lineno,
					path not in to_function,
			)) for path in _tracer_include):
				pass

			# Filter by black-list
			elif _tracer_exclude and any(any((
				# path in from_path,
				# path in from_lineno,
				# path in from_function,
				path in to_path,
				path in to_lineno,
				path in to_function,
			)) for path in _tracer_exclude):
				pass

			# Show trace
			else:
				if _tracer_breakpoints_are_enabled and _tracer_breakpoints and any(any((
						breakpoint in to_function,
				)) for breakpoint in _tracer_breakpoints):
					_tracer_is_paused = True

				frame = to_frame
				for level in range(sys.getrecursionlimit()):
					if frame is None:
						break
					frame = frame.f_back

				thread_name = threading.current_thread().name
				thread_name = 'M' if thread_name == 'MainThread' else thread_name[-1]

				if 0:
					# Get line from source
					from_line = linecache.getline(from_path, from_lineno).strip()
					from_line

				from_dirname = from_path[:-len(from_filename)]
				to_dirname = to_path[:-len(to_filename)]

				# Make paths relative to current path
				if _tracer_prefix is not None:
					if from_dirname.startswith(_tracer_prefix):
						from_dirname = from_dirname[len(_tracer_prefix):]
					if to_dirname.startswith(_tracer_prefix):
						to_dirname = to_dirname[len(_tracer_prefix):]

				if 0:
					# Shorten paths
					if len(from_dirname) >= 50:
						from_dirname = from_dirname[:12] + "..." + from_dirname[-35:]
					if len(to_dirname) >= 50:
						to_dirname = to_dirname[:12] + "..." + to_dirname[-35:]

				if 0:
					# Remove extension. Breaks the transfer to the source
					from_filename = from_filename.split('.', 1)[0]
					to_filename = to_filename.split('.', 1)[0]

				# Hint: the nicest syntax-highlight of trace-info for Vim: set ft=sh
				print >>sys.stderr, ''.join((
					# "       ",
					# "│  " * (level + _tracer_offset),
					# "│",
					# " " * (len(to_function) + len(from_filename) + len(str(from_lineno))),
					# "\n",
					"{:.6f}".format(time.time() - _tracer_previous_timestamp).lstrip('0')[:7],
					" {:s}".format(thread_name),
					"{:.<80}".format(
						''.join((
							"|  " * (level + _tracer_offset),
							"┡━━┓",
							to_function,
							"()"
						))
					),
					# "{:.<40}".format(
					"{:}".format(
						''.join((
							"`",
							from_dirname + from_filename,
							":",
							str(from_lineno),
							" => ",
							(to_dirname + to_filename) if (from_dirname + from_filename) != (to_dirname + to_filename) else "",
							":",
							to_lineno,
							"`",
						))
					),
					# "{:.<110}".format(
					#     ''.join((
					#         "'",
					#         from_line,
					#         "'",
					#         ))
					#     ),
					# "{:}".format(
					#     ''.join((
					#         "[",
					#         from_dirname,
					#         " => " if from_dirname != to_dirname else "",
					#         to_dirname if from_dirname != to_dirname else "",
					#         "]",
					#         ))
					#     ),
				)),
				sys.stderr.flush()

				if _tracer_sleep:
					_tracer_sleep_event.clear()
					_tracer_sleep_event.wait(timeout=_tracer_sleep)

				if _tracer_is_paused:
					os.system("stty raw")
					print "b",
					sys.stderr.flush()
					command = sys.stdin.readline()
					if command.startswith('n'):
						_tracer_is_paused = False
					if command.startswith('c'):
						_tracer_breakpoints_are_enabled = False
						_tracer_is_paused = False

				print
				sys.stderr.flush()

				_tracer_previous_timestamp = time.time()

	return trace_callback if not debug_callback else debug_callback

# Set up the trace-callback
sys.settrace(trace_callback)
threading.settrace(trace_callback)

_profiler = None


def start_profiler():
	global _profiler

	from cProfile import Profile
	_profiler = Profile()
	_profiler.enable()


def stop_profiler(filename):
	_profiler.dump_stats(filename)


_line_profiler = None


def start_line_profiler():
	global _line_profiler

	from helpers.line_profiler import LineProfiler
	_line_profiler = LineProfiler()

	return _line_profiler


def stop_line_profiler(filename_prefix):
	stats = _line_profiler.get_stats()
	profile = {}
	for (module_path, function_lineno, function_name), values in stats.timings.items():
		total = sum(value[2] for value in values)
		profile.setdefault(module_path, []).extend([(value[0], value[1], value[2], total) for value in values])
	for value in profile.values():
		value.sort()
	for module_path, values in profile.items():
		filename = os.path.split(module_path)[-1]
		profile_filename = '{}_{}.prof'.format(filename_prefix, filename.replace('.', '_'))
		print >>sys.stderr, "profile_filename,", profile_filename; sys.stderr.flush()  # FIXME: must be removed
		with open(profile_filename, 'w') as f:
			for index in range(1, 9999):
				if values and index == values[0][0]:
					lineno, count, timing, total = values.pop(0)
					timing_in_mcs = int(timing * stats.unit * 1000000)
					total_in_mcs = int(total * stats.unit * 1000000)
					print >>f, '{:.2%}, {} = {} / {}'.format(
						timing_in_mcs / total_in_mcs,
						timing_in_mcs // count,
						timing_in_mcs,
						count,
					),
				print >>f, ''


def debugger(type_):
	"""Sets debugger type for debugger"""

	global _debugger_type
	_debugger_type = type_


def function(function):
	"""Sets function for debugger"""

	global _is_debugger_enabled
	global _debugger_function
	_debugger_function = function
	_is_debugger_enabled = True


def line(line):
	"""Sets line (<str>:<int>) for debugger"""

	global _is_debugger_enabled
	global _debugger_filename
	global _debugger_lineno
	_debugger_filename, _debugger_lineno = line.split(":", 1)
	print _debugger_filename, _debugger_lineno
	_is_debugger_enabled = True


def include(*args):
	"""Appends elements for white-list for tracer"""

	_tracer_include.extend(args)


def exclude(*args):
	"""Appends elements for black-list for tracer"""

	_tracer_exclude.extend(args)


def line_by_line(value):
	"""Enables trace of every line, not only entrance into the function"""

	global _tracer_line_by_line
	_tracer_line_by_line = value


def breakpoints(*args):
	"""Appends elements to breakpoints for tracer. Obsoleted."""

	_tracer_breakpoints.extend(args)


def tracer_enable(state=True):
	"""Enables tracer"""

	global _is_tracer_enabled
	_is_tracer_enabled = state


def tracer_disable(state=True):
	"""Disables tracer"""

	global _is_tracer_enabled
	_is_tracer_enabled = not state


def bind_to_wx():
	"""Binds hotkeys to WX-Application

	F1 -- interactive console.

	Events loop must be activated.
	"""

	def _on_key_down(event):
		global _is_debugger_enabled
		global _debugger_type
		global _tracer_sleep
		global _tracer_breakpoints_are_enabled

		code = event.GetKeyCode()
		# control = event.ControlDown()
		# alt = event.AltDown()
		# shift = event.ShiftDown()
		# if control and alt and code == ord('T'):
		# import sys; print "code:", code; sys.stderr.flush() # FIXME: must be removed
		# if control and code == ord('T') and 0:

		# if code == 341:  # F2
		# if code == 342:  # F3
		# if code == 343:  # F4
		if code == 340:  # F1
			while True:
				sys.stderr.write("tracer > ")
				sys.stderr.flush()

				command = sys.stdin.readline().rstrip()

				if not command:
					continue
				elif command in ("exit", "continue", "c"):
					break

				elif command in ("tracer enable", "te"):
					tracer_enable()
					print "Tracer is enabled"

				elif command in ("tracer disable", "td"):
					tracer_disable()
					print "Tracer is disabled"

				elif command.startswith(("sleep=", "s=")):
					_tracer_sleep = int(command.split("=", 1)[1])
					print "Sleep={:d}".format(_tracer_sleep)

				elif command in ("debugger enable", "de"):
					_is_debugger_enabled = True
					print "Debugger is enabled"

				elif command in ("debugger disable", "dd"):
					_is_debugger_enabled = False
					print "Debugger is disabled"

				elif command.startswith(("debugger type=", "dtype=", "dt=")):
					_debugger_type = command.split("=", 1)[1]
					print "DebuggerType={:}".format(_debugger_type)

				elif command.startswith(("filename=", "f=")):
					filename_with_lineno = command.split("=", 1)[1]
					global _debugger_filename
					global _debugger_lineno
					try:
						_debugger_filename, _debugger_lineno = filename_with_lineno.split(":", 1)
					except Exception:
						print "Wrong format (must be <str>:<int>)"
					else:
						print "File={:}:{:}".format(_debugger_filename, _debugger_lineno)

				elif command.startswith(("function=", "fn=")):
					global _debugger_function
					_debugger_function = command.split("=", 1)[1]
					print "Function={:s}".format(_debugger_function)

				else:
					print "Wrong command"

				sys.stderr.flush()

			event.StopPropagation()

	# Bind event-handler
	import wx
	wx.GetApp().Bind(wx.EVT_KEY_DOWN, _on_key_down)


def main():

	# Test for print_stack:
	if 0:
		# tracer_enable()
		def a():
			b()

		def b():
			c()

		def c():
			pass
			print_stack()

		a()

	# Test for dump_object:
	if 0:
		class Test(object):
			pass

		class OtherTest(object):
			pass

		test = Test()
		test.the_same_test = test
		test.test_attribute = 'test attribute'
		test.other_test = OtherTest()
		test.other_test.test_attribute = 'other\ntest attribute'

		dump_object(test, depth=3)

	# Test for tracer
	if 1:
		tracer_enable()
		include('my_trace')
		line_by_line(True)
		# exclude('598')
		# exclude('loop')
		# include('_core')

		def test2():
			pass
			pass
			pass
			pass

		def test1():
			pass
			pass
			test2()
			pass
			pass

		test1()

	# Test for debugger
	if 0:
		# debugger("wx")
		line("my_trace:644")
		# function("loop")

	# Test for bind_to_wx:
	if 0:
		import wx
		app = wx.App()
		frame = wx.Frame(None)
		frame.SetSizer(wx.BoxSizer(wx.VERTICAL))

		button = wx.Button(parent=frame, label="test")
		frame.GetSizer().Add(button)

		frame.Show()

		print 'main'
		print 'main'
		print 'main'
		print 'main'
		print 'main'
		print 'main'
		print 'main'

		def loop():
			test = 1
			print 'loop'
			print 'loop'
			print 'loop'
			print 'loop'
			print 'loop'
			print 'loop'
			print 'loop'
			print 'loop'
			print 'loop'
			sys.stderr.flush()
			test = 2
			test

			wx.CallLater(1000, loop)
		wx.CallAfter(loop)

		bind_to_wx()

		app.MainLoop()


if __name__ == '__main__':
	main()
