QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT += svg

TARGET = board_game_lord_of_the_rings_trunk
TEMPLATE = app

APP_NAME=Board Game: Lord of the Rings
PACKAGE=gehrmann.board.game.lord.of.the.rings
VERSION=0.01a
VERSION_CODE=1

HEADERS += init_python.h
SOURCES += main.cpp init_python.cpp
RESOURCES += images.qrc
FORMS    += \
	views/main_view.ui \
	views/map_view.ui \

CONFIG += mobility
MOBILITY = 

DISTFILES += \
    Android/AndroidManifest.xml \
    Android/gradle/wrapper/gradle-wrapper.jar \
    Android/gradlew \
    Android/res/values/libs.xml \
    Android/build.gradle \
    Android/gradle/wrapper/gradle-wrapper.properties \
    Android/gradlew.bat

CONFIG(debug, debug|release) {
	PACKAGE=			$${PACKAGE}.debug
	APP_NAME+=			(debug)
}
CONFIG(banner) {
	PACKAGE=			$${PACKAGE}.free
	APP_NAME+=			(free)
} else {
	CONFIG(full) {
		PACKAGE=			$${PACKAGE}.full
		APP_NAME+=			(full)
	} else {
		PACKAGE=			$${PACKAGE}.pro
		APP_NAME+=			(pro)
	}
}

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {

	# Custom java-source for banner support
	android_package.target = .android_package/AndroidManifest.xml
	android_package.depends += $$PWD/Android/AndroidManifest.xml
	android_package.depends += $$PWD/QtActivity.java
	android_package.depends += $$PWD/QtLoader.java
	android_package.commands += echo \'==============================\';
	android_package.commands += echo \'Building $$android_package.target\';
	android_package.commands += rm -rf .android_package;
	android_package.commands += mkdir -p .android_package;
	android_package.commands += cp -R $$PWD/Android/res .android_package/;
	android_package.commands += cp $$PWD/Android/AndroidManifest.xml .android_package/;
	android_package.commands += sed -i.bak \'s/-- %%INSERT_PACKAGE_NAME%% --/$${PACKAGE}/\' .android_package/AndroidManifest.xml;
	android_package.commands += sed -i.bak \'s/-- %%INSERT_APP_VERSION_NAME%% --/$${VERSION}/\' .android_package/AndroidManifest.xml;
	android_package.commands += sed -i.bak \'s/-- %%INSERT_APP_VERSION_CODE%% --/$${VERSION_CODE}/\' .android_package/AndroidManifest.xml;
	android_package.commands += sed -i.bak \'s/-- %%INSERT_APP_NAME%% --/$${APP_NAME}/g\' .android_package/AndroidManifest.xml;
	android_package.commands += mkdir -p .android_package/src/org/qtproject/qt5/android/bindings/;
	android_package.commands += cp $$PWD/QtActivity.java .android_package/src/org/qtproject/qt5/android/bindings/;
	android_package.commands += cp $$PWD/QtLoader.java .android_package/src/org/qtproject/qt5/android/bindings/;
	android_package.commands += echo Done;
	CONFIG(banner) | CONFIG(broadcast_receiver) {
		android_package.commands += sed -i.bak \'s%</activity>%</activity>\n\n\t\t<!-- Google play services library -->\n\t\t<meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\"/>\n\t\t<activity android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" android:name=\"com.google.android.gms.ads.AdActivity\"/>\n\t\t<!-- Google play services library -->\n%\' .android_package/AndroidManifest.xml;
		android_package.commands += ln -s $$PWD/.deps/android/all/libs/google-play-services_lib .android_package/google-play-services;
		android_package.commands += echo \'android.library.reference.1=google-play-services\' >> .android_package/project.properties;
	}
	CONFIG(banner) {
		android_package.commands += echo \'==============================\';
		android_package.commands += echo \'Adding Banner\';
		android_package.depends += $$PWD/QtBanner.java
		android_package.commands += cp $$PWD/QtBanner.java .android_package/src/org/qtproject/qt5/android/bindings/;
		android_package.commands += echo Done;
	} else {
		android_package.commands += sed -i.bak \'/\\/\\/ BANNER/d\' .android_package/src/org/qtproject/qt5/android/bindings/QtActivity.java;
		android_package.commands += sed -i.bak \'/\\/\\/ BANNER/d\' .android_package/src/org/qtproject/qt5/android/bindings/QtLoader.java;
	}
	CONFIG(broadcast_receiver) {
		android_package.depends += $$PWD/QtBroadcastReceiver.java
		android_package.commands += cp $$PWD/QtBroadcastReceiver.java .android_package/src/org/qtproject/qt5/android/bindings/;
	}
	QMAKE_EXTRA_TARGETS += android_package
	PRE_TARGETDEPS += $$android_package.target
	ANDROID_PACKAGE_SOURCE_DIR = .android_package

	INCLUDEPATH += $$PWD/.deps/all/python/.build/python2.7.2-android-arm
	INCLUDEPATH += $$PWD/.deps/all/python/.build/python2.7.2-android-arm/Include
 	LIBS += -L$$PWD/.deps/all/python/.build/python2.7.2-android-arm -lpython2.7
    ANDROID_EXTRA_LIBS += $$PWD/.deps/all/python/.build/python2.7.2-android-arm/libpython2.7.so

	# Pythons base
	python.target = .armeabi-v7a-libs/lib+python27+.so
	python.depends += $$PWD/.deps/android/all/assets/python27.zip
	python.commands += mkdir -p .armeabi-v7a-libs;
	python.commands += rm -rf $@;
	python.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += python
	PRE_TARGETDEPS += $$python.target
    ANDROID_EXTRA_LIBS += $$python.target

	# Pythons modules
	_collections.target += .armeabi-v7a-libs/lib+_collections.so+.so
	_collections.depends += $$PWD/.deps/android/all/assets/lib-dynload/_collections.so
	_collections.commands = mkdir -p .armeabi-v7a-libs/;
	_collections.commands += rm -rf $@;
	_collections.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _collections
	PRE_TARGETDEPS += $$_collections.target
	ANDROID_EXTRA_LIBS += $$_collections.target

	_elementtree.target += .armeabi-v7a-libs/lib+_elementtree.so+.so
	_elementtree.depends += $$PWD/.deps/android/all/assets/lib-dynload/_elementtree.so
	_elementtree.commands = mkdir -p .armeabi-v7a-libs/;
	_elementtree.commands += rm -rf $@;
	_elementtree.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _elementtree
	PRE_TARGETDEPS += $$_elementtree.target
	ANDROID_EXTRA_LIBS += $$_elementtree.target

	_functools.target += .armeabi-v7a-libs/lib+_functools.so+.so
	_functools.depends += $$PWD/.deps/android/all/assets/lib-dynload/_functools.so
	_functools.commands = mkdir -p .armeabi-v7a-libs/;
	_functools.commands += rm -rf $@;
	_functools.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _functools
	PRE_TARGETDEPS += $$_functools.target
	ANDROID_EXTRA_LIBS += $$_functools.target

	# _hashlib.target += .armeabi-v7a-libs/lib+_hashlib.so+.so
	# _hashlib.depends += $$PWD/.deps/android/all/assets/lib-dynload/_hashlib.so
	# _hashlib.commands = mkdir -p .armeabi-v7a-libs/;
	# _hashlib.commands += rm -rf $@;
	# _hashlib.commands += cp $^ $@;
	# QMAKE_EXTRA_TARGETS += _hashlib
	# PRE_TARGETDEPS += $$_hashlib.target
	# ANDROID_EXTRA_LIBS += $$_hashlib.target

	_io.target += .armeabi-v7a-libs/lib+_io.so+.so
	_io.depends += $$PWD/.deps/android/all/assets/lib-dynload/_io.so
	_io.commands = mkdir -p .armeabi-v7a-libs/;
	_io.commands += rm -rf $@;
	_io.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _io
	PRE_TARGETDEPS += $$_io.target
	ANDROID_EXTRA_LIBS += $$_io.target

	_lsprof.target += .armeabi-v7a-libs/lib+_lsprof.so+.so
	_lsprof.depends += $$PWD/.deps/android/all/assets/lib-dynload/_lsprof.so
	_lsprof.commands = mkdir -p .armeabi-v7a-libs/;
	_lsprof.commands += rm -rf $@;
	_lsprof.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _lsprof
	PRE_TARGETDEPS += $$_lsprof.target
	ANDROID_EXTRA_LIBS += $$_lsprof.target

	_md5module.target += .armeabi-v7a-libs/lib+_md5module.so+.so
	_md5module.depends += $$PWD/.deps/all/python/.build/python2.7.2-android-arm/Modules/_md5module.so
	_md5module.commands = mkdir -p .armeabi-v7a-libs/;
	_md5module.commands += rm -rf $@;
	_md5module.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _md5module
	PRE_TARGETDEPS += $$_md5module.target
	ANDROID_EXTRA_LIBS += $$_md5module.target

	_random.target += .armeabi-v7a-libs/lib+_random.so+.so
	_random.depends += $$PWD/.deps/android/all/assets/lib-dynload/_random.so
	_random.commands = mkdir -p .armeabi-v7a-libs/;
	_random.commands += rm -rf $@;
	_random.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _random
	PRE_TARGETDEPS += $$_random.target
	ANDROID_EXTRA_LIBS += $$_random.target

	_shamodule.target += .armeabi-v7a-libs/lib+_shamodule.so+.so
	_shamodule.depends += $$PWD/.deps/all/python/.build/python2.7.2-android-arm/Modules/_shamodule.so
	_shamodule.commands = mkdir -p .armeabi-v7a-libs/;
	_shamodule.commands += rm -rf $@;
	_shamodule.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _shamodule
	PRE_TARGETDEPS += $$_shamodule.target
	ANDROID_EXTRA_LIBS += $$_shamodule.target

	_sha256module.target += .armeabi-v7a-libs/lib+_sha256module.so+.so
	_sha256module.depends += $$PWD/.deps/all/python/.build/python2.7.2-android-arm/Modules/_sha256module.so
	_sha256module.commands = mkdir -p .armeabi-v7a-libs/;
	_sha256module.commands += rm -rf $@;
	_sha256module.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _sha256module
	PRE_TARGETDEPS += $$_sha256module.target
	ANDROID_EXTRA_LIBS += $$_sha256module.target

	_sha512module.target += .armeabi-v7a-libs/lib+_sha512module.so+.so
	_sha512module.depends += $$PWD/.deps/all/python/.build/python2.7.2-android-arm/Modules/_sha512module.so
	_sha512module.commands = mkdir -p .armeabi-v7a-libs/;
	_sha512module.commands += rm -rf $@;
	_sha512module.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _sha512module
	PRE_TARGETDEPS += $$_sha512module.target
	ANDROID_EXTRA_LIBS += $$_sha512module.target

	_socket.target += .armeabi-v7a-libs/lib+_socket.so+.so
	_socket.depends += $$PWD/.deps/android/all/assets/lib-dynload/_socket.so
	_socket.commands = mkdir -p .armeabi-v7a-libs/;
	_socket.commands += rm -rf $@;
	_socket.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _socket
	PRE_TARGETDEPS += $$_socket.target
	ANDROID_EXTRA_LIBS += $$_socket.target

	# _sqlite3.target += .armeabi-v7a-libs/lib+_sqlite3.so+.so
	# _sqlite3.depends += $$PWD/.deps/android/all/assets/lib-dynload/_sqlite3.so
	# _sqlite3.commands = mkdir -p .armeabi-v7a-libs/;
	# _sqlite3.commands += rm -rf $@;
	# _sqlite3.commands += cp $^ $@;
	# QMAKE_EXTRA_TARGETS += _sqlite3
	# PRE_TARGETDEPS += $$_sqlite3.target
	# ANDROID_EXTRA_LIBS += $$_sqlite3.target

	_struct.target += .armeabi-v7a-libs/lib+_struct.so+.so
	_struct.depends += $$PWD/.deps/android/all/assets/lib-dynload/_struct.so
	_struct.commands = mkdir -p .armeabi-v7a-libs/;
	_struct.commands += rm -rf $@;
	_struct.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _struct
	PRE_TARGETDEPS += $$_struct.target
	ANDROID_EXTRA_LIBS += $$_struct.target

	array.target += .armeabi-v7a-libs/lib+array.so+.so
	array.depends += $$PWD/.deps/android/all/assets/lib-dynload/array.so
	array.commands = mkdir -p .armeabi-v7a-libs/;
	array.commands += rm -rf $@;
	array.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += array
	PRE_TARGETDEPS += $$array.target
	ANDROID_EXTRA_LIBS += $$array.target

	binascii.target += .armeabi-v7a-libs/lib+binascii.so+.so
	binascii.depends += $$PWD/.deps/android/all/assets/lib-dynload/binascii.so
	binascii.commands = mkdir -p .armeabi-v7a-libs/;
	binascii.commands += rm -rf $@;
	binascii.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += binascii
	PRE_TARGETDEPS += $$binascii.target
	ANDROID_EXTRA_LIBS += $$binascii.target

	cPickle.target += .armeabi-v7a-libs/lib+cPickle.so+.so
	cPickle.depends += $$PWD/.deps/android/all/assets/lib-dynload/cPickle.so
	cPickle.commands = mkdir -p .armeabi-v7a-libs/;
	cPickle.commands += rm -rf $@;
	cPickle.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += cPickle
	PRE_TARGETDEPS += $$cPickle.target
	ANDROID_EXTRA_LIBS += $$cPickle.target

	cStringIO.target += .armeabi-v7a-libs/lib+cStringIO.so+.so
	cStringIO.depends += $$PWD/.deps/android/all/assets/lib-dynload/cStringIO.so
	cStringIO.commands = mkdir -p .armeabi-v7a-libs/;
	cStringIO.commands += rm -rf $@;
	cStringIO.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += cStringIO
	PRE_TARGETDEPS += $$cStringIO.target
	ANDROID_EXTRA_LIBS += $$cStringIO.target

	datetime.target += .armeabi-v7a-libs/lib+datetime.so+.so
	datetime.depends += $$PWD/.deps/android/all/assets/lib-dynload/datetime.so
	datetime.commands = mkdir -p .armeabi-v7a-libs/;
	datetime.commands += rm -rf $@;
	datetime.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += datetime
	PRE_TARGETDEPS += $$datetime.target
	ANDROID_EXTRA_LIBS += $$datetime.target

	future_builtins.target += .armeabi-v7a-libs/lib+future_builtins.so+.so
	future_builtins.depends += $$PWD/.deps/android/all/assets/lib-dynload/future_builtins.so
	future_builtins.commands = mkdir -p .armeabi-v7a-libs/;
	future_builtins.commands += rm -rf $@;
	future_builtins.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += future_builtins
	PRE_TARGETDEPS += $$future_builtins.target
	ANDROID_EXTRA_LIBS += $$future_builtins.target

	itertools.target += .armeabi-v7a-libs/lib+itertools.so+.so
	itertools.depends += $$PWD/.deps/android/all/assets/lib-dynload/itertools.so
	itertools.commands = mkdir -p .armeabi-v7a-libs/;
	itertools.commands += rm -rf $@;
	itertools.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += itertools
	PRE_TARGETDEPS += $$itertools.target
	ANDROID_EXTRA_LIBS += $$itertools.target

	math.target += .armeabi-v7a-libs/lib+math.so+.so
	math.depends += $$PWD/.deps/android/all/assets/lib-dynload/math.so
	math.commands = mkdir -p .armeabi-v7a-libs/;
	math.commands += rm -rf $@;
	math.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += math
	PRE_TARGETDEPS += $$math.target
	ANDROID_EXTRA_LIBS += $$math.target

	operator.target += .armeabi-v7a-libs/lib+operator.so+.so
	operator.depends += $$PWD/.deps/android/all/assets/lib-dynload/operator.so
	operator.commands = mkdir -p .armeabi-v7a-libs/;
	operator.commands += rm -rf $@;
	operator.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += operator
	PRE_TARGETDEPS += $$operator.target
	ANDROID_EXTRA_LIBS += $$operator.target

	parser.target += .armeabi-v7a-libs/lib+parser.so+.so
	parser.depends += $$PWD/.deps/android/all/assets/lib-dynload/parser.so
	parser.commands = mkdir -p .armeabi-v7a-libs/;
	parser.commands += rm -rf $@;
	parser.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += parser
	PRE_TARGETDEPS += $$parser.target
	ANDROID_EXTRA_LIBS += $$parser.target

	pyexpat.target += .armeabi-v7a-libs/lib+pyexpat.so+.so
	pyexpat.depends += $$PWD/.deps/android/all/assets/lib-dynload/pyexpat.so
	pyexpat.commands = mkdir -p .armeabi-v7a-libs/;
	pyexpat.commands += rm -rf $@;
	pyexpat.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += pyexpat
	PRE_TARGETDEPS += $$pyexpat.target
	ANDROID_EXTRA_LIBS += $$pyexpat.target

	time.target += .armeabi-v7a-libs/lib+time.so+.so
	time.depends += $$PWD/.deps/android/all/assets/lib-dynload/time.so
	time.commands = mkdir -p .armeabi-v7a-libs/;
	time.commands += rm -rf $@;
	time.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += time
	PRE_TARGETDEPS += $$time.target
	ANDROID_EXTRA_LIBS += $$time.target

	unicodedata.target += .armeabi-v7a-libs/lib+unicodedata.so+.so
	unicodedata.depends += $$PWD/.deps/android/all/assets/lib-dynload/unicodedata.so
	unicodedata.commands = mkdir -p .armeabi-v7a-libs/;
	unicodedata.commands += rm -rf $@;
	unicodedata.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += unicodedata
	PRE_TARGETDEPS += $$unicodedata.target
	ANDROID_EXTRA_LIBS += $$unicodedata.target

	# zlib.target += .armeabi-v7a-libs/lib+zlib.so+.so
	# zlib.depends += $$PWD/.deps/android/all/assets/lib-dynload/zlib.so
	# zlib.commands = mkdir -p .armeabi-v7a-libs/;
	# zlib.commands += rm -rf $@;
	# zlib.commands += cp $^ $@;
	# QMAKE_EXTRA_TARGETS += zlib
	# PRE_TARGETDEPS += $$zlib.target
	# ANDROID_EXTRA_LIBS += $$zlib.target

	_ssl.target += .armeabi-v7a-libs/lib+_ssl.so+.so
	_ssl.depends += $$PWD/.deps/all/python/.build/python2.7.2-android-arm/Modules/_ssl.so
	_ssl.commands = mkdir -p .armeabi-v7a-libs/;
	_ssl.commands += rm -rf $@;
	_ssl.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += _ssl
	PRE_TARGETDEPS += $$_ssl.target
	ANDROID_EXTRA_LIBS += $$_ssl.target

	sip.target += .armeabi-v7a-libs/lib+sip.so+.so
	sip.depends += $$PWD/.deps/all/sip/.build/.android-arm/siplib/sip.so
	sip.commands = mkdir -p .armeabi-v7a-libs/;
	sip.commands += rm -rf $@;
	sip.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += sip
	PRE_TARGETDEPS += $$sip.target
	ANDROID_EXTRA_LIBS += $$sip.target

	pyqt5_init.target += .armeabi-v7a-libs/lib+PyQt5+__init__.py+.so
	pyqt5_init.depends += $$PWD/.deps/android/all/assets/site-packages/PyQt5/__init__.py
	pyqt5_init.commands = mkdir -p .armeabi-v7a-libs/;
	pyqt5_init.commands += rm -rf $@;
	pyqt5_init.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += pyqt5_init
	PRE_TARGETDEPS += $$pyqt5_init.target
	ANDROID_EXTRA_LIBS += $$pyqt5_init.target

	pyqt5_qt.target += .armeabi-v7a-libs/lib+PyQt5+Qt.so+.so
	pyqt5_qt.depends += $$PWD/.deps/all/pyqt5/.build/.android-arm/Qt/Qt.so
	pyqt5_qt.commands = mkdir -p .armeabi-v7a-libs/;
	pyqt5_qt.commands += rm -rf $@;
	pyqt5_qt.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += pyqt5_qt
	PRE_TARGETDEPS += $$pyqt5_qt.target
	ANDROID_EXTRA_LIBS += $$pyqt5_qt.target

	pyqt5_qtcore.target += .armeabi-v7a-libs/lib+PyQt5+QtCore.so+.so
	pyqt5_qtcore.depends += $$PWD/.deps/all/pyqt5/.build/.android-arm/QtCore/QtCore.so
	pyqt5_qtcore.commands = mkdir -p .armeabi-v7a-libs/;
	pyqt5_qtcore.commands += rm -rf $@;
	pyqt5_qtcore.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += pyqt5_qtcore
	PRE_TARGETDEPS += $$pyqt5_qtcore.target
	ANDROID_EXTRA_LIBS += $$pyqt5_qtcore.target

	pyqt5_qtwidgets.target += .armeabi-v7a-libs/lib+PyQt5+QtWidgets.so+.so
	pyqt5_qtwidgets.depends += $$PWD/.deps/all/pyqt5/.build/.android-arm/QtWidgets/QtWidgets.so
	pyqt5_qtwidgets.commands = mkdir -p .armeabi-v7a-libs/;
	pyqt5_qtwidgets.commands += rm -rf $@;
	pyqt5_qtwidgets.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += pyqt5_qtwidgets
	PRE_TARGETDEPS += $$pyqt5_qtwidgets.target
	ANDROID_EXTRA_LIBS += $$pyqt5_qtwidgets.target

	pyqt5_qtgui.target += .armeabi-v7a-libs/lib+PyQt5+QtGui.so+.so
	pyqt5_qtgui.depends += $$PWD/.deps/all/pyqt5/.build/.android-arm/QtGui/QtGui.so
	pyqt5_qtgui.commands = mkdir -p .armeabi-v7a-libs/;
	pyqt5_qtgui.commands += rm -rf $@;
	pyqt5_qtgui.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += pyqt5_qtgui
	PRE_TARGETDEPS += $$pyqt5_qtgui.target
	ANDROID_EXTRA_LIBS += $$pyqt5_qtgui.target

	pyqt5_qtsvg.target += .armeabi-v7a-libs/lib+PyQt5+QtSvg.so+.so
	pyqt5_qtsvg.depends += $$PWD/.deps/all/pyqt5/.build/.android-arm/QtSvg/QtSvg.so
	pyqt5_qtsvg.commands = mkdir -p .armeabi-v7a-libs/;
	pyqt5_qtsvg.commands += rm -rf $@;
	pyqt5_qtsvg.commands += cp $^ $@;
	QMAKE_EXTRA_TARGETS += pyqt5_qtsvg
	PRE_TARGETDEPS += $$pyqt5_qtsvg.target
	ANDROID_EXTRA_LIBS += $$pyqt5_qtsvg.target

	app.target = .armeabi-v7a-libs/lib+app+.so
	app.depends += $$PWD/controllers/*.py
	app.depends += $$PWD/models/*.py
	app.depends += $$PWD/helpers/*.py
	app.depends += $$PWD/views/*.ui
	app.depends += $$PWD/images.py
	app.depends += $$PWD/styles.py
	app.commands += mkdir -p .armeabi-v7a-libs;
	app.commands += echo \'==============================\';
	app.commands += echo Building $$app.target;
	app.commands += rm -rf .armeabi-v7a-libs/app $$app.target;
	app.commands += mkdir .armeabi-v7a-libs/app;
	app.commands += cp -R $$PWD/controllers .armeabi-v7a-libs/app;
	app.commands += cp -R $$PWD/models .armeabi-v7a-libs/app;
	app.commands += cp -R $$PWD/helpers .armeabi-v7a-libs/app;
	app.commands += mkdir .armeabi-v7a-libs/app/views && cp -R $$PWD/views/*.ui .armeabi-v7a-libs/app/views;
	app.commands += cp -R $$PWD/images.py .armeabi-v7a-libs/app;
	app.commands += cp -R $$PWD/styles.py .armeabi-v7a-libs/app;
	app.commands += python -m compileall .armeabi-v7a-libs/app;
	app.commands += find .armeabi-v7a-libs/app -name '*.py' -o -name "*.pyo" -o -name "*.pyx" -type f | xargs rm -rf;
	app.commands += cd .armeabi-v7a-libs/app && zip -r ../app.zip . && cd ../..;
	app.commands += rm -rf .armeabi-v7a-libs/app;
	app.commands += mv .armeabi-v7a-libs/app.zip .armeabi-v7a-libs/lib+app+.so;
	QMAKE_EXTRA_TARGETS += app
	PRE_TARGETDEPS += $$app.target
    ANDROID_EXTRA_LIBS += $$app.target
}
